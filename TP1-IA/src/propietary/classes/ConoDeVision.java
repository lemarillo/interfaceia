package propietary.classes;

import java.util.ArrayList;

import propietary.tiles.TileFactory;
import propietary.tiles.Tiles;
import propietary.tiles.TilesType;

public class ConoDeVision {
	
	private ConoNode[][] conoNode;
	char orientation;
	private Position agentRealPosition;
	private Position agentConoVisionPos;
	private Position sarahConnor;
	private boolean hasSarahConnor;
	int width;
	int lenght;
	private boolean[][] visibles;
	
	public ConoDeVision(Position t800Position, int visionLevel,char orient){
		orientation= orient;
		agentRealPosition = t800Position;
		sarahConnor = Config.instance().getSarahConnorPosition();
		hasSarahConnor = false;
		
		
		//int half = ((visionLevel*2 +1) /2) +1;
		int half = ((visionLevel*2) /2 -1);
		
		Tiles unk = TileFactory.getTile(TilesType.UNKNOWN,false);
		
		switch(orientation){
		case 'N':
			agentConoVisionPos = new Position(half,visionLevel);
			break;
		case 'E':
			agentConoVisionPos = new Position(0,half);
			break;
		case 'S':
			agentConoVisionPos = new Position(half,0);
			break;
		case 'W':
			agentConoVisionPos = new Position(visionLevel,half);
			break;
		}
		
		if(orientation == 'N' || orientation =='S'){
			width = visionLevel*2 -1;
			lenght = visionLevel + 1;
			this.conoNode = new ConoNode[lenght][width];
			this.visibles = new boolean[lenght][width];
		}else{
			width = visionLevel + 1;
			lenght = visionLevel*2 -1;
			this.conoNode = new ConoNode[lenght][width];
			this.visibles = new boolean[lenght][width]; 
		}
		
//		for(boolean[] row : this.visibles)
//			for(boolean value : row)
//				value=true;
		
		for(int r=0; r<lenght;r++){ // la r es Y y la c es X 
			for(int c=0;c<width;c++){
				Position diffWithAgent = Position.difference(new Position(c,r), agentConoVisionPos); // Antes estaba como cr (?)
				Position realPosOfrc = Position.sum(agentRealPosition, diffWithAgent);
				
				ConoNode aux = new ConoNode(realPosOfrc,unk);
				
				this.conoNode[r][c] = aux;
				this.visibles[r][c] = true;
			}
		}

	}
	private ConoDeVision(){
		super();
	}
	@Override
	public boolean equals(Object obj){
		if(obj.getClass().equals(this.getClass())){
			ConoDeVision cv = (ConoDeVision)obj;
			if(this.agentConoVisionPos.equals(cv.agentConoVisionPos) && this.agentRealPosition.equals(cv.agentRealPosition) 
					&& this.sarahConnor.equals(cv.sarahConnor) && this.hasSarahConnor == cv.hasSarahConnor 
					&& this.orientation == cv.orientation && this.width == cv.width && this.lenght==cv.lenght){
				boolean equalFlag = true;
				for(int r = 0; r<lenght;r++){
					for(int c=0; c<width;c++){
						if(this.conoNode[r][c].equals(cv.conoNode[r][c]) == false){
							equalFlag = false;
						}
					}
					
				}
				return equalFlag;
			}
		}
		return false;
	}
	@Override
	public ConoDeVision clone(){
		ConoDeVision aux = new ConoDeVision();
		aux.agentRealPosition = this.agentRealPosition.clone();
		aux.agentConoVisionPos = this.agentConoVisionPos.clone();
		aux.sarahConnor = this.sarahConnor.clone();
		aux.width = this.width;
		aux.lenght = this.lenght;
		aux.hasSarahConnor = this.hasSarahConnor;
		
		aux.orientation = this.orientation;
		
		aux.visibles = new boolean[lenght][width];
		aux.conoNode = new ConoNode[lenght][width];
		for(int r=0; r<lenght;r++){
			for(int c=0; c<width;c++){
				aux.visibles[r][c] = this.visibles[r][c];
				aux.conoNode[r][c] = this.conoNode[r][c].clone();
			}
		}
		return aux;
	}
	
	public void agregarAlCono (ConoNode ct){
		Position diffWithAgent = Position.difference(ct.getPosition(), agentRealPosition);
		Position matrixPosition = Position.sum(agentConoVisionPos, diffWithAgent);
		
		conoNode[matrixPosition.getY()][matrixPosition.getX()] = ct; 
		
		//Tiles tile = ct.getTile();
		
//		if(tile.hasObstacle() || tile.wallFront(orientation) || tile.wallLeft(orientation) || tile.wallRight(orientation)){
//			occlude(matrixPosition);
//		}
		
		if(ct.hasSarahConnor()){
			this.sarahConnor= new Position(matrixPosition.getX(),matrixPosition.getY());
			this.hasSarahConnor=true;
		}
		
		
		
	}
	
	
	private void occlude(Position matrixPosition) {
		// TODO Auto-generated method stub
		int typeOfOcclusion = typeOfOcclusion(matrixPosition);
		int diag;
		switch(orientation){
		case 'N':
			switch(typeOfOcclusion){
			case -1:
				//izquierda
				diag = 1;
				for(int r = matrixPosition.getY() - 1; r>=0;r--){
					for(int c = matrixPosition.getX();c>0;c--){
						if(r <= (matrixPosition.getY()- diag) && c >= (matrixPosition.getX() - diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				break;
			case 1:
				//derecha
				diag = 1;
				for(int r = matrixPosition.getY() - 1; r>=0;r--){
					for(int c = matrixPosition.getX();c < width;c++){
						if(r <= (matrixPosition.getY()- diag) && c <= (matrixPosition.getX() - diag)){
							this.visibles[r][c] = false;
						}
					}
				}
				break;
			case 0:
				//izquierda y derecha
				diag = 1;
				for(int r = matrixPosition.getY() - 1; r>=0;r--){
					for(int c = matrixPosition.getX();c>0;c--){
						if(r <= (matrixPosition.getY()- diag) && c >= (matrixPosition.getX() - diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				diag = 1;
				for(int r = matrixPosition.getY() - 1; r>=0;r--){
					for(int c = matrixPosition.getX();c < width;c++){
						if(r <= (matrixPosition.getY()- diag) && c <= (matrixPosition.getX() - diag)){
							this.visibles[r][c] = false;
						}
					}
					diag++;
				}
				
				break;
			}
			break;
		case 'E':
			switch(typeOfOcclusion){
			case -1:
				diag = 1;
				for(int r = matrixPosition.getY();r>=0;r--){
					for(int c = matrixPosition.getX() + 1; c < width;c++){
						if(r >= (matrixPosition.getY()- diag) && c >= (matrixPosition.getX() + diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				break;
			case 1:
				diag = 1;
				for(int r = matrixPosition.getY();r<lenght;r++){
					for(int c = matrixPosition.getX() + 1; c < width;c++){
						if(r <= (matrixPosition.getY()- diag) && c >= (matrixPosition.getX() + diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				break;
			case 0:
				diag = 1;
				for(int r = matrixPosition.getY();r>=0;r--){
					for(int c = matrixPosition.getX() + 1; c < width;c++){
						if(r >= (matrixPosition.getY()- diag) && c >= (matrixPosition.getX() + diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				diag = 1;
				for(int r = matrixPosition.getY();r<lenght;r++){
					for(int c = matrixPosition.getX() + 1; c < width;c++){
						if(r <= (matrixPosition.getY()- diag) && c >= (matrixPosition.getX() + diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				break;
			}
			break;
		case 'S':
			switch(typeOfOcclusion){
			case -1:
				diag = 1;
				for(int r = matrixPosition.getY()+1;r<lenght;r++){
					for(int c = matrixPosition.getX(); c < width; c++){
						if(r >= (matrixPosition.getY()+ diag) && c <= (matrixPosition.getX() + diag))
							this.visibles[r][c] = false;	
					}
					diag++;
				}
				break;
			case 1:
				diag = 1;
				for(int r = matrixPosition.getY()+1;r<lenght;r++){
					for(int c = matrixPosition.getX();c>=0; c--){
						if(r >= (matrixPosition.getY()+ diag) && c >= (matrixPosition.getX() - diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				break;
			case 0:
				diag = 1;
				for(int r = matrixPosition.getY()+1;r<lenght;r++){
					for(int c = matrixPosition.getX(); c < width; c++){
						if(r >= (matrixPosition.getY()+ diag) && c <= (matrixPosition.getX() + diag))
							this.visibles[r][c] = false;	
					}
					diag++;
				}
				diag = 1;
				for(int r = matrixPosition.getY()+1;r<lenght;r++){
					for(int c = matrixPosition.getX();c>=0; c--){
						if(r >= (matrixPosition.getY()+ diag) && c >= (matrixPosition.getX() - diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				break;
			}
			break;
		case 'W':
			switch(typeOfOcclusion){
			case -1:
				diag = 1;
				for(int r = matrixPosition.getY(); r<lenght;r++){
					for(int c = matrixPosition.getX() - 1; c>=0;c--){
						if(r <= (matrixPosition.getY()+ diag) && c <= (matrixPosition.getX() - diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				break;
			case 1:
				diag=1;
				for(int r = matrixPosition.getY(); r>=0;r--){
					for(int c = matrixPosition.getX() - 1; c>=0;c--){
						if(r >= (matrixPosition.getY()- diag) && c <= (matrixPosition.getX() - diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				break;
			case 0:
				diag = 1;
				for(int r = matrixPosition.getY(); r<lenght;r++){
					for(int c = matrixPosition.getX() - 1; c>=0;c--){
						if(r <= (matrixPosition.getY()+ diag) && c <= (matrixPosition.getX() - diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				diag=1;
				for(int r = matrixPosition.getY(); r>=0;r--){
					for(int c = matrixPosition.getX() - 1; c>=0;c--){
						if(r >= (matrixPosition.getY()- diag) && c <= (matrixPosition.getX() - diag))
							this.visibles[r][c] = false;
					}
					diag++;
				}
				break;
			}
			break;
		}
		
	}
	private int typeOfOcclusion(Position pos){
		
		int diff;
		
		switch(orientation){
		case 'N':
			diff = Position.difference(pos, agentConoVisionPos).getX();
			
			if(diff > 0)
				return 1; //esta a la derecha
			else if (diff < 0)
				return -1; // esta a la izquierda
			else
				return 0; // esta en el centro
		case 'E':
			diff = Position.difference(pos, agentConoVisionPos).getY();
			
			if(diff > 0)
				return 1; //esta a la derecha
			else if (diff < 0)
				return -1; // esta a la izquierda
			else
				return 0; // esta en el centro
		case 'S':
			diff = Position.difference(pos, agentConoVisionPos).getX();
			
			if(diff > 0)
				return -1; //esta a la izquierda
			else if (diff < 0)
				return 1; // esta a la derecha
			else
				return 0; // esta en el centro
		case 'W':
			diff = Position.difference(pos, agentConoVisionPos).getY();
			
			if(diff > 0)
				return -1; //esta a la izquierda
			else if (diff < 0)
				return 1; // esta a la derecha
			else
				return 0; // esta en el centro
		}
		return 29081997;
		
	}
	
	public ConoNode[][] leerCono(){
		
		ConoNode[][] aux;
		
//		if(orientation == 'N' || orientation =='S'){
//			aux = new ConoNode[lenght][width];
//		}else{
//			aux = new ConoNode[lenght][width];
//		}
		aux = new ConoNode[lenght][width];
		
		Tiles unk = TileFactory.getTile(TilesType.UNKNOWN,false);
		
		for(int r = 0 ; r< lenght; r++){
			for(int c = 0; c < width; c++){
				if(this.visibles[r][c]){
					aux[r][c] = this.conoNode[r][c];
				}else{
					aux[r][c] = new ConoNode(this.conoNode[r][c].getPosition(),unk);
				}
	
			}
		}
		
		return aux;
	}
	public boolean hasSarahConnor() {
		// TODO Auto-generated method stub
		return hasSarahConnor;
	}
	public Position getSarahPosition(){
		return this.sarahConnor;
	}
	
	@Override
	public String toString(){
		String aux = new String();
		for(ConoNode[] lcn : conoNode){
			for(ConoNode c: lcn){
				aux+= (c.toString() + ", ");
			}
			aux += "\n";
			
		}
		return aux;
	}

}
