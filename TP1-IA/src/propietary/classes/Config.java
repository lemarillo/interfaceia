package propietary.classes;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import frsf.cidisi.exercise.t800.search.T800Agent;

import propietary.tiles.TileFactory;
import propietary.tiles.Tiles;

public class Config {
	
	private static Config instance= null;
	private int row;
	private int column;
	private int t800ambient;
	private Position sarahConnor;
	private int sarahConnorAmbient;
	private Position t800;
	private char t800Orientation;
	private ArrayList<Position> obstacles;
	private ArrayList<ArrayList<Tiles>> world;
	private int visionLevel;
	
	private boolean hardcodeado = true;
	
	private Config(){
		if(hardcodeado){
//			row=7;
			row=42;
//			column =7;
			column =26;

			t800ambient = 4;
			t800Orientation = 'N';
//			t800 = new Position(8,3);
//			sarahConnor = new Position(11,8);
			sarahConnorAmbient = 5;
			visionLevel = 2;
			
			obstacles = new ArrayList<Position>();
		}else{
			//leer del archivo
		}
		ArrayList<Position> t800andSara = new ArrayList<Position>();
		
		
		//GENERA EL MUNDO LEYENDO DEL ARCHIVO CSV.
		try {
			//String[][] strMap = LectoWriter.matrixReader(new FileInputStream("C:/Users/leand/Downloads/Matrizreducida1.csv"));
			String[][] strMap = LectoWriter.matrixReader(new FileInputStream("../world.csv"));

			world = new ArrayList<ArrayList<Tiles>>();
			
			for(String[] sl : strMap){
				ArrayList<Tiles> tileRow = new ArrayList<Tiles>();
				for(String s : sl){
					tileRow.add(TileFactory.getTile(s));
				}
				world.add(tileRow);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//CARGA LOS OBSTACULOS DEL ARCHIVO CSV
		FileInputStream ArchivoObstaculos;
		try {
			ArchivoObstaculos = new FileInputStream("../obstacles.csv");
			this.obstacles = LectoWriter.readObstacle(ArchivoObstaculos);
		} catch (FileNotFoundException e) {
			System.out.println("NO LEE EL ARCHIVO");
			e.printStackTrace();
		}
		
		
		//LEE LA POSICION INICIAL DEL T800 Y DE SARAH CONNOR
		try {
			ArchivoObstaculos = new FileInputStream("../positions.csv");
			
			t800andSara = LectoWriter.readObstacle(ArchivoObstaculos);
		} catch (FileNotFoundException e) {
			System.out.println("NO LEE EL ARCHIVO");
			e.printStackTrace();
		}
		this.t800=t800andSara.get(0);
		this.sarahConnor=t800andSara.get(1);
		
	}
	
	
	public static Config instance(){
		if(instance == null)
			instance = new Config();
		return instance;
	}
	public int getRow() {
		return row;
	}
	public int getColumn() {
		return column;
	}
	public int getT800ambient() {
		return t800ambient;
	}
	public Position getSarahConnorPosition() {
		return sarahConnor;
	}
	public int getSarahConnorAmbient() {
		return sarahConnorAmbient;
	}
	public Position getT800Position() {
		return t800;
	}
	public char getT800Orientation() {
		return t800Orientation;
	}
	public ArrayList<Position> getObstacles() {
		return obstacles;
	}
	public ArrayList<ArrayList<Tiles>> getWorld() {
		return world;
	}
	public int getVisionLevel() {
		return visionLevel;
	}
	
	
}
