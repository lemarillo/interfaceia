package propietary.classes;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import propietary.tiles.TileFactory;
import propietary.tiles.Tiles;



	public class LectoWriter {
		
	public static final String SEPARATOR=";";
	public static final String QUOTE="\"";
	
	private static LectoWriter instance;
	//private FileInputStream inputRoute;
	//private static FileWriter fw;
	
	LectoWriter(){
		//Acordarse de modificar
		//this.inputRoute= new FileInputStream ("C:/Users/Bernal German/Desktop/Matrizcsv.csv");
		//this.fw = new FileWriter("C:/Users/Bernal German/Desktop/pruevaEscupir.csv",true);
	}
	
	public static LectoWriter Instance(){
		
		if(instance == null)
			instance = new LectoWriter();
		
		return instance;
	}
	
	public static String[][] matrixReader (FileInputStream archivo){
		   
		   String[][] matrizString = new String[42][26];
		   //String[][] matrizString = new String[7][7];

		  
		   BufferedReader br = null;
		   
		    try {
		    
		         br =new BufferedReader(new InputStreamReader(archivo, "UTF-8"));
		         String line = br.readLine();
		         line = br.readLine(); //Saltea la basura del principio del csv
		         int i=0;
		         while (line!=null) {
		        	int j=0;
		            String [] fields = line.split(SEPARATOR);
		            for (String s: fields) {
		            	matrizString[i][j] = s;
		            	j++;        	
		            }
		            line = br.readLine();
		            i++;
		         }    
		      } catch (Exception e) {
		    	  System.out.println("fallo");
		      } finally {
		         if (null!=br) {
		            try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
		         }
		      }
		   return matrizString; 
	   }
	
	 public static Tiles[][] matrixStringToTiles(String[][] matString){
		 	int fila =Config.instance().getRow();
		 	int columna = Config.instance().getColumn();
		   Tiles[][] world = new Tiles[fila][columna];
		   	for (int i=0; i<fila; i++){
		   		for (int j=0; j<columna; j++){
					world[i][j] = TileFactory.getTile(matString[i][j]);
		   		}
			}
		   return world;
	   }
	 
	 public static void spitToFile (String s) throws IOException{
		  
		  PrintWriter pw= new PrintWriter (new FileWriter("../results.csv",true));
		  StringBuilder sb =new StringBuilder();
		  sb.append(s);
		  sb.append(";");
		  pw.write(sb.toString());
		  pw.close();
	   }
	 
	 public static ArrayList<Position> readObstacle(FileInputStream archivo){
	 ArrayList<Position> obstaclePositions = new ArrayList<Position>();
	 String [] fields ;
	 String [] fields2;
	 String [] fields3= null;
	 ArrayList<String> aux = new ArrayList<String>();
	 BufferedReader br = null;
	 try {
			br =new BufferedReader(new InputStreamReader(archivo, "UTF-8"));
		 	String line = br.readLine();
		 	line = br.readLine(); //Saltea la basura del principio del csv
		 	while (line!=null) {
	           fields = line.split(SEPARATOR);
	            
	            for (String s: fields){
	              	fields2 = s.split("\\)");
	            	for (String s2: fields2){
	            		fields3 = s2.split("\\(");
	            		for( String s3 : fields3){
	            			aux.add(s3);
	            			}
	            		}
	            }
	         line = br.readLine();
	         }
//		 	System.out.println("empezo a leer");
//
//		 	aux.remove("");
//		 	for (String a: aux){
//		 		System.out.println(a);
//		 	}
//		 	System.out.println("termino de leer");
	 	} 
	 	catch (Exception e) {
	    	  System.out.println("fallo en readObstacle");
	      	}finally {
	         if (null!=br) {
	            try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
	         }
	      	}
	      	
	      for (String str: aux){
	    		if(!str.equals("")){
	    		  obstaclePositions.add(new Position(str));  
	    		}
	      }
      for (Position posObs: obstaclePositions){
    	 // System.out.println(posObs.toString());	    
    	  }
	 return obstaclePositions;
 }
}
