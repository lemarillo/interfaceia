package propietary.classes;

import propietary.tiles.TileFactory;
import propietary.tiles.Tiles;
import propietary.tiles.TilesType;

public class ConoNode {
	private Position position;
	private Tiles tile;
	private boolean hasSarahConnor;
	
	
	public ConoNode(Position pos, Tiles til) {
		position = pos;
		tile = til;
		if(Config.instance().getSarahConnorPosition().equals(pos))
			hasSarahConnor = true;
	}
	
	public Position getPosition() {
		return position;
	}
	public Tiles getTile() {
		return tile;
	}
	
	public void setUnknown(){
		tile = TileFactory.getTile(TilesType.UNKNOWN, false);
	}

	public boolean hasSarahConnor() {
		return hasSarahConnor;
	}
	@Override
	public ConoNode clone(){
		return new ConoNode(this.position.clone(),this.tile);
	}
	
	@Override
	public String toString(){
		return "["+this.position.toString()+", "+this.tile.toString()+"]";
		
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj.getClass().equals(this.getClass())){
			ConoNode cn = (ConoNode) obj;
			return cn.hasSarahConnor==this.hasSarahConnor && cn.position.equals(this.position) && cn.tile.equals(this.tile);
		}
		return false;
	}
	
}
