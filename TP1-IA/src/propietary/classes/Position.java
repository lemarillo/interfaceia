package propietary.classes;

public class Position{
	
	private int posX;
	private int posY;
	
	public Position(int x,int y){
		this.posX=x;
		this.posY=y;
	}
	
	public int getX(){
		return this.posX;
	}
	public int getY(){
		return this.posY;
	}
	
	public void changePosition(int x,int y){
		this.posX=x;
		this.posY=y;
	}
	
	@Override
	public boolean equals(Object o){
		if(o.getClass() == this.getClass()){
			Position op = (Position)o;
			return (op.posX == this.posX) && (op.posY == this.posY); 
		}else{
			return false;
		}
	}
	@Override
	public Position clone(){
		return new Position(this.posX,this.posY);
	}
	
	public static Position difference(Position pos2, Position pos1){
		return new Position(pos2.getX()- pos1.getX(),pos2.getY()-pos1.getY());
	}
	
	public static Position sum(Position pos1, Position pos2){
		return new Position(pos1.posX + pos2.posX, pos1.posY + pos2.posY);
	}
	
	@Override
	public String toString(){
		return "("+this.posX+","+this.posY+")";
	}
	
	public Position (String str){
		String[] aux =str.split(",");
		this.posX=Integer.parseInt(aux[0]);
		this.posY=Integer.parseInt(aux[1]);
	}
	
}