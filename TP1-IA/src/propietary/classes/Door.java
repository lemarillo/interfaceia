package propietary.classes;

public class Door {
	private int ambientA;
	private int ambientB;
	private boolean isOpened;
	
	Door(int ambA, int ambB, boolean opened){
		ambientA = ambA;
		ambientB = ambB;
		isOpened = opened;
	}
	
	public int getAmbientA() {
		return ambientA;
	}
	public int getAmbientB() {
		return ambientB;
	}
	public boolean isOpened() {
		return isOpened;
	}
	public void setOpened(boolean isOpened) {
		this.isOpened = isOpened;
	}
	
	@Override
	public boolean equals(Object o){
		if(o.getClass() == this.getClass()){
			Door od= (Door) o;
			return ((this.ambientA == od.ambientA) && (this.ambientB == od.ambientB) && (this.isOpened == od.isOpened));
		}else{
			return false;
		}
	}
	
	@Override
	public Door clone(){
		return new Door(this.ambientA,this.ambientB,this.isOpened);
	}
}
