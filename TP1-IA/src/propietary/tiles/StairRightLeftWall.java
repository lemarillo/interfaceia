package propietary.tiles;

public class StairRightLeftWall extends Tiles {
	StairRightLeftWall(){
		super();
		this.velocityCostUp = 1.5f;
		this.velocityCostDown = 6;
		this.velocityCostLeft = 3;
		this.velocityCostRight = 3;
		this.allowMoveDown = true;
		this.allowMoveUp = false;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	
	@Override
	public String toString(){
		return "SRLeW";
	}
}
