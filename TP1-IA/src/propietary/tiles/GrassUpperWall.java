package propietary.tiles;


public class GrassUpperWall extends Tiles {
	GrassUpperWall(){
		super();
		this.velocityCostUp = 6;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = false;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "GUW";
	}
}
