package propietary.tiles;


public class NormalUpperLeftWall extends Tiles {

	NormalUpperLeftWall(){
		super();
		this.velocityCostUp = 3;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = false;
		this.allowMoveLeft = false;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "NULeW";
	}
}
