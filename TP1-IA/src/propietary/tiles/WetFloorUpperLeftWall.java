package propietary.tiles;


public class WetFloorUpperLeftWall extends Tiles {

	WetFloorUpperLeftWall(){
		super();
		this.velocityCostUp = 1.5f;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = false;
		this.allowMoveLeft = false;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "WULeW";
	}
}
