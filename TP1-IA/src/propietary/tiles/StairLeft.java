package propietary.tiles;

public class StairLeft extends Tiles {
	StairLeft(){
		super();
		this.velocityCostUp = 3;
		this.velocityCostDown = 3;
		this.velocityCostLeft = 6;
		this.velocityCostRight = 1.5f;
		this.allowMoveDown = true;
		this.allowMoveUp = false;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	
	@Override
	public String toString(){
		return "SLe";
	}
}
