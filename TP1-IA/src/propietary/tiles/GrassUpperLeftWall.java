package propietary.tiles;


public class GrassUpperLeftWall extends Tiles {

	GrassUpperLeftWall(){
		super();
		this.velocityCostUp = 6;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = false;
		this.allowMoveLeft = false;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "GULeW";
	}
}
