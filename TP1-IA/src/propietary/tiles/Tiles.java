package propietary.tiles;

public abstract class Tiles {
	
	protected float velocityCostUp;
	protected float velocityCostDown;
	protected float velocityCostLeft;
	protected float velocityCostRight;
	protected boolean allowMoveUp;
	protected boolean allowMoveDown;
	protected boolean allowMoveLeft;
	protected boolean allowMoveRight;
	protected boolean obstacle;
	
	Tiles(){
		
	}
	
	public boolean hasObstacle(){
		return this.obstacle;
	}
	public void setObstacle(){
		this.obstacle = true;
	}
	
	public void cleanObstacle(){
		this.obstacle = false;
	}
	
	public boolean canMove(char direction){
		switch(direction){
		case'N':
			return this.allowMoveUp;
		case'E':
			return this.allowMoveRight;
		case'S':
			return this.allowMoveDown;
		case'W':
			return this.allowMoveLeft;
		default:
			return false;
		}
	}
	public boolean wallRight(char direction){
		switch(direction){
		case'N':
			return !this.allowMoveRight;
		case'E':
			return !this.allowMoveDown;
		case'S':
			return !this.allowMoveLeft;
		case'W':
			return !this.allowMoveUp;
		default:
			return true;
		}
	}
	public boolean wallFront(char direction){
		switch(direction){
		case'N':
			return !this.allowMoveUp;
		case'E':
			return !this.allowMoveRight;
		case'S':
			return !this.allowMoveDown;
		case'W':
			return !this.allowMoveLeft;
		default:
			return true;
		}
	}
	public boolean wallLeft(char direction){
		switch(direction){
		case'N':
			return !this.allowMoveLeft;
		case'E':
			return !this.allowMoveUp;
		case'S':
			return !this.allowMoveRight;
		case'W':
			return !this.allowMoveDown;
		default:
			return true;
		}
	}
	
	public String toString(){
		return null;
	}
	
	@Override
	public boolean equals(Object o){
		if(o.getClass() == this.getClass()){
			Tiles ot = (Tiles)o;
			return  (this.allowMoveDown == ot.allowMoveDown) &&
					(this.allowMoveLeft == ot.allowMoveLeft) &&
					(this.allowMoveRight == ot.allowMoveRight) &&
					(this.allowMoveUp == ot.allowMoveUp) &&
					(this.velocityCostUp == ot.velocityCostUp) &&
					(this.velocityCostDown == ot.velocityCostDown) &&
					(this.velocityCostLeft == ot.velocityCostLeft) &&
					(this.velocityCostRight == ot.velocityCostRight) &&
					(this.obstacle == ot.obstacle);
		}else{
			return false;
		}
	}
	public float getVelocityCost(char direction){
			
			float velocityCost=0;
			switch(direction){
				case 'N':
					velocityCost = this.velocityCostUp;
					break;
				case 'E':
					velocityCost = this.velocityCostRight;
					break;
				case 'S':
					velocityCost = this.velocityCostDown;
					break;
				case 'W':
					velocityCost = this.velocityCostLeft;
					break;
					
			}
			return velocityCost;
	}
	
}
