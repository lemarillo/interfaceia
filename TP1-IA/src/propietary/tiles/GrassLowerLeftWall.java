package propietary.tiles;


public class GrassLowerLeftWall extends Tiles {

	GrassLowerLeftWall(){
		super();
		this.velocityCostUp = 6;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = false;
		this.allowMoveUp = true;
		this.allowMoveLeft = false;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "GLoLeW";
	}
}
