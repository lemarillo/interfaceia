package propietary.tiles;

public class StairLower extends Tiles {
	StairLower(){
		super();
		this.velocityCostUp = 1.5f;
		this.velocityCostDown = 6;
		this.velocityCostLeft = 3;
		this.velocityCostRight = 3;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	
	@Override
	public String toString(){
		return "SLo";
	}
}
