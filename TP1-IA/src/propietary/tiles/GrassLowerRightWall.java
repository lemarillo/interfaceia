package propietary.tiles;


public class GrassLowerRightWall extends Tiles {

	GrassLowerRightWall(){
		super();
		this.velocityCostUp = 6;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = false;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = false;

	}
	@Override
	public String toString(){
		return "GLoRW";
	}
}
