package propietary.tiles;


public class GrassUpperRightWall extends Tiles {

	GrassUpperRightWall(){
		super();
		this.velocityCostUp = 6;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = false;
		this.allowMoveLeft = true;
		this.allowMoveRight = false;

	}
	@Override
	public String toString(){
		return "GURW";
	}
}
