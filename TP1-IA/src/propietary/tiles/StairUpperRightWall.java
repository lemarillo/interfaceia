package propietary.tiles;

public class StairUpperRightWall extends Tiles {
	StairUpperRightWall(){
		super();
		this.velocityCostUp = 6;
		this.velocityCostDown = 1.5f;
		this.velocityCostLeft = 3;
		this.velocityCostRight = 3;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = false;

	}
	
	@Override
	public String toString(){
		return "SURW";
	}
}
