package propietary.tiles;


public class Normal extends Tiles {

	Normal(){
		super();
		this.velocityCostUp = 3;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "N";
	}
}
