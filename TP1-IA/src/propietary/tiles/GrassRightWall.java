package propietary.tiles;


public class GrassRightWall extends Tiles {
	GrassRightWall(){
		super();
		this.velocityCostUp = 2;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = false;

	}
	@Override
	public String toString(){
		return "GRW";
	}
}
