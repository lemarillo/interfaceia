package propietary.tiles;


public class NormalUpperRightWall extends Tiles {

	NormalUpperRightWall(){
		super();
		this.velocityCostUp = 3;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = false;
		this.allowMoveLeft = true;
		this.allowMoveRight = false;

	}
	@Override
	public String toString(){
		return "NURW";
	}
}
