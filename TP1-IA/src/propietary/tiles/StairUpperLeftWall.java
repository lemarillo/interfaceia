package propietary.tiles;

public class StairUpperLeftWall extends Tiles {
	StairUpperLeftWall(){
		super();
		this.velocityCostUp = 6;
		this.velocityCostDown = 1.5f;
		this.velocityCostLeft = 3;
		this.velocityCostRight = 3;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = false;
		this.allowMoveRight = true;

	}
	
	@Override
	public String toString(){
		return "SULeW";
	}
}
