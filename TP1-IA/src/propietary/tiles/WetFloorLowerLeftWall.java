package propietary.tiles;


public class WetFloorLowerLeftWall extends Tiles {

	WetFloorLowerLeftWall(){
		super();
		this.velocityCostUp = 1.5f;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = false;
		this.allowMoveUp = true;
		this.allowMoveLeft = false;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "WLoLeW";
	}
}
