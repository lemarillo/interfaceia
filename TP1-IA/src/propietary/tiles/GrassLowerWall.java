package propietary.tiles;


public class GrassLowerWall extends Tiles {
	GrassLowerWall(){
		super();
		this.velocityCostUp = 6;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = false;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "GLoW";
	}
}
