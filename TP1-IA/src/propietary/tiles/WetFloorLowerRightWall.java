package propietary.tiles;


public class WetFloorLowerRightWall extends Tiles {

	WetFloorLowerRightWall(){
		super();
		this.velocityCostUp = 1.5f;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = false;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = false;

	}
	@Override
	public String toString(){
		return "WLoRW";
	}
}
