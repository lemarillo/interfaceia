package propietary.tiles;


public class NormalLowerWall extends Tiles {
	NormalLowerWall(){
		super();
		this.velocityCostUp = 1;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = false;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "NLoW";
	}
}
