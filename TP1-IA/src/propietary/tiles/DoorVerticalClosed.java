package propietary.tiles;

public class DoorVerticalClosed extends Tiles {
	DoorVerticalClosed(){
		super();
		this.velocityCostUp = 1000;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = false;
		this.allowMoveUp = false;
		this.allowMoveLeft = false;
		this.allowMoveRight = false;

	}
	
	@Override
	public String toString(){
		return "DVC";
	}
}
