package propietary.tiles;

public class StairUpper extends Tiles {
	StairUpper(){
		super();
		this.velocityCostUp = 6;
		this.velocityCostDown = 1.5f;
		this.velocityCostLeft = 3;
		this.velocityCostRight = 3;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	
	@Override
	public String toString(){
		return "SU";
	}
}
