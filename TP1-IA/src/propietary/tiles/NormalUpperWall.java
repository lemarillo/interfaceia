package propietary.tiles;


public class NormalUpperWall extends Tiles {
	NormalUpperWall(){
		super();
		this.velocityCostUp = 3;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = false;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "NUW";
	}
}
