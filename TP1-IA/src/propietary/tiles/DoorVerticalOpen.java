package propietary.tiles;

public class DoorVerticalOpen extends Tiles {
	DoorVerticalOpen(){
		super();
		this.velocityCostUp = 3;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = false;
		this.allowMoveRight = false;

	}
	
	@Override
	public String toString(){
		return "DVO";
	}
}
