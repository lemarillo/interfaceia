package propietary.tiles;

public class WetFloor extends Tiles {
	WetFloor(){
		super();
		this.velocityCostUp = 1.5f;
		this.velocityCostDown = this.velocityCostUp;
		this.velocityCostLeft = this.velocityCostUp;
		this.velocityCostRight = this.velocityCostUp;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	@Override
	public String toString(){
		return "W";
	}
}
