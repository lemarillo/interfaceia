package propietary.tiles;

public class StairLeftLeftWall extends Tiles {
	StairLeftLeftWall(){
		super();
		this.velocityCostUp = 3;
		this.velocityCostDown = 3;
		this.velocityCostLeft = 6;
		this.velocityCostRight = 1.5f;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = true;
		this.allowMoveRight = true;

	}
	
	@Override
	public String toString(){
		return "SLeLeW";
	}
}
