package propietary.tiles;

import java.util.HashMap;

public class TileFactory {
	
	private static TileFactory instance;
	private HashMap<TilesType,Tiles> cleanTiles;
	private HashMap<TilesType,Tiles> obstructedTiles;
	
	private TileFactory(){
		cleanTiles = new HashMap<TilesType,Tiles>();
		obstructedTiles = new HashMap<TilesType,Tiles>();
		
		cleanTiles.put(TilesType.GRASS, new Grass());
		cleanTiles.put(TilesType.GRASS_LEFT_WALL, new GrassLeftWall());
		cleanTiles.put(TilesType.GRASS_LOWER_LEFT_WALL, new GrassLowerLeftWall());
		cleanTiles.put(TilesType.GRASS_LOWER_RIGHT_WALL, new GrassLowerRightWall());
		cleanTiles.put(TilesType.GRASS_LOWER_WALL, new GrassLowerWall());
		cleanTiles.put(TilesType.GRASS_RIGHT_WALL, new GrassRightWall());
		cleanTiles.put(TilesType.GRASS_UPPER_LEFT_WALL, new GrassUpperLeftWall());
		cleanTiles.put(TilesType.GRASS_UPPER_RIGHT_WALL, new GrassUpperRightWall());
		cleanTiles.put(TilesType.GRASS_UPPER_WALL, new GrassUpperWall());
		
		cleanTiles.put(TilesType.NORMAL, new Normal());
		cleanTiles.put(TilesType.NORMAL_LEFT_WALL, new NormalLeftWall());
		cleanTiles.put(TilesType.NORMAL_LOWER_LEFT_WALL, new NormalLowerLeftWall());
		cleanTiles.put(TilesType.NORMAL_LOWER_RIGHT_WALL, new NormalLowerRightWall());
		cleanTiles.put(TilesType.NORMAL_LOWER_WALL, new NormalLowerWall());
		cleanTiles.put(TilesType.NORMAL_RIGHT_WALL, new NormalRightWall());
		cleanTiles.put(TilesType.NORMAL_UPPER_LEFT_WALL, new NormalUpperLeftWall());
		cleanTiles.put(TilesType.NORMAL_UPPER_RIGHT_WALL, new NormalUpperRightWall());
		cleanTiles.put(TilesType.NORMAL_UPPER_WALL, new NormalUpperWall());
		
		cleanTiles.put(TilesType.RUG, new Rug());
		cleanTiles.put(TilesType.RUG_LEFT_WALL, new RugLeftWall());
		cleanTiles.put(TilesType.RUG_LOWER_LEFT_WALL, new RugLowerLeftWall());
		cleanTiles.put(TilesType.RUG_LOWER_RIGHT_WALL, new RugLowerRightWall());
		cleanTiles.put(TilesType.RUG_LOWER_WALL, new RugLowerWall());
		cleanTiles.put(TilesType.RUG_RIGHT_WALL, new RugRightWall());
		cleanTiles.put(TilesType.RUG_UPPER_LEFT_WALL, new RugUpperLeftWall());
		cleanTiles.put(TilesType.RUG_UPPER_RIGHT_WALL, new RugUpperRightWall());
		cleanTiles.put(TilesType.RUG_UPPER_WALL, new RugUpperWall());
		
		cleanTiles.put(TilesType.WET, new WetFloor());
		cleanTiles.put(TilesType.WET_LEFT_WALL, new WetFloorLeftWall());
		cleanTiles.put(TilesType.WET_LOWER_LEFT_WALL, new WetFloorLowerLeftWall());
		cleanTiles.put(TilesType.WET_LOWER_RIGHT_WALL, new WetFloorLowerRightWall());
		cleanTiles.put(TilesType.WET_LOWER_WALL, new WetFloorLowerWall());
		cleanTiles.put(TilesType.WET_RIGHT_WALL, new WetFloorRightWall());
		cleanTiles.put(TilesType.WET_UPPER_LEFT_WALL, new WetFloorUpperLeftWall());
		cleanTiles.put(TilesType.WET_UPPER_RIGHT_WALL, new WetFloorUpperRightWall());
		cleanTiles.put(TilesType.WET_UPPER_WALL, new WetFloorUpperWall());
		
		cleanTiles.put(TilesType.STAIR_LEFT, new StairLeft());
		cleanTiles.put(TilesType.STAIR_LEFT_LEFT_WALL, new StairLeftLeftWall());
		cleanTiles.put(TilesType.STAIR_LEFT_RIGHT_WALL, new StairLeftRightWall());
		
		cleanTiles.put(TilesType.STAIR_RIGHT, new StairRight());
		cleanTiles.put(TilesType.STAIR_RIGHT_LEFT_WALL, new StairRightLeftWall());
		cleanTiles.put(TilesType.STAIR_RIGHT_RIGHT_WALL, new StairRightRightWall());
		
		cleanTiles.put(TilesType.STAIR_UPPER, new StairUpper());
		cleanTiles.put(TilesType.STAIR_UPPER_LEFT_WALL, new StairUpperLeftWall());
		cleanTiles.put(TilesType.STAIR_UPPER_RIGHT_WALL, new StairUpperRightWall());
		
		cleanTiles.put(TilesType.STAIR_LOWER, new StairLower());
		cleanTiles.put(TilesType.STAIR_LOWER_LEFT_WALL, new StairLowerLeftWall());
		cleanTiles.put(TilesType.STAIR_LOWER_RIGHT_WALL, new StairLowerRightWall());
		
		cleanTiles.put(TilesType.DOOR_HORIZONTAL_CLOSED, new DoorHorizontalClosed());
		cleanTiles.put(TilesType.DOOR_HORIZONTAL_OPEN, new DoorHorizontalOpen());
		
		cleanTiles.put(TilesType.DOOR_VERTICAL_CLOSED, new DoorVerticalClosed());
		cleanTiles.put(TilesType.DOOR_VERTICAL_OPEN, new DoorVerticalOpen());
		cleanTiles.put(TilesType.UNKNOWN, new Unknown());

		
		//cleanTiles.put(TilesType.UNKNOWN, new Unknown());
		
		//obstructed tiles
		obstructedTiles.put(TilesType.GRASS, new Grass());
		obstructedTiles.put(TilesType.GRASS_LEFT_WALL, new GrassLeftWall());
		obstructedTiles.put(TilesType.GRASS_LOWER_LEFT_WALL, new GrassLowerLeftWall());
		obstructedTiles.put(TilesType.GRASS_LOWER_RIGHT_WALL, new GrassLowerRightWall());
		obstructedTiles.put(TilesType.GRASS_LOWER_WALL, new GrassLowerWall());
		obstructedTiles.put(TilesType.GRASS_RIGHT_WALL, new GrassRightWall());
		obstructedTiles.put(TilesType.GRASS_UPPER_LEFT_WALL, new GrassUpperLeftWall());
		obstructedTiles.put(TilesType.GRASS_UPPER_RIGHT_WALL, new GrassUpperRightWall());
		obstructedTiles.put(TilesType.GRASS_UPPER_WALL, new GrassUpperWall());
		
		obstructedTiles.put(TilesType.NORMAL, new Normal());
		obstructedTiles.put(TilesType.NORMAL_LEFT_WALL, new NormalLeftWall());
		obstructedTiles.put(TilesType.NORMAL_LOWER_LEFT_WALL, new NormalLowerLeftWall());
		obstructedTiles.put(TilesType.NORMAL_LOWER_RIGHT_WALL, new NormalLowerRightWall());
		obstructedTiles.put(TilesType.NORMAL_LOWER_WALL, new NormalLowerWall());
		obstructedTiles.put(TilesType.NORMAL_RIGHT_WALL, new NormalRightWall());
		obstructedTiles.put(TilesType.NORMAL_UPPER_LEFT_WALL, new NormalUpperLeftWall());
		obstructedTiles.put(TilesType.NORMAL_UPPER_RIGHT_WALL, new NormalUpperRightWall());
		obstructedTiles.put(TilesType.NORMAL_UPPER_WALL, new NormalUpperWall());
		
		obstructedTiles.put(TilesType.RUG, new Rug());
		obstructedTiles.put(TilesType.RUG_LEFT_WALL, new RugLeftWall());
		obstructedTiles.put(TilesType.RUG_LOWER_LEFT_WALL, new RugLowerLeftWall());
		obstructedTiles.put(TilesType.RUG_LOWER_RIGHT_WALL, new RugLowerRightWall());
		obstructedTiles.put(TilesType.RUG_LOWER_WALL, new RugLowerWall());
		obstructedTiles.put(TilesType.RUG_RIGHT_WALL, new RugRightWall());
		obstructedTiles.put(TilesType.RUG_UPPER_LEFT_WALL, new RugUpperLeftWall());
		obstructedTiles.put(TilesType.RUG_UPPER_RIGHT_WALL, new RugUpperRightWall());
		obstructedTiles.put(TilesType.RUG_UPPER_WALL, new RugUpperWall());
		
		obstructedTiles.put(TilesType.WET, new WetFloor());
		obstructedTiles.put(TilesType.WET_LEFT_WALL, new WetFloorLeftWall());
		obstructedTiles.put(TilesType.WET_LOWER_LEFT_WALL, new WetFloorLowerLeftWall());
		obstructedTiles.put(TilesType.WET_LOWER_RIGHT_WALL, new WetFloorLowerRightWall());
		obstructedTiles.put(TilesType.WET_LOWER_WALL, new WetFloorLowerWall());
		obstructedTiles.put(TilesType.WET_RIGHT_WALL, new WetFloorRightWall());
		obstructedTiles.put(TilesType.WET_UPPER_LEFT_WALL, new WetFloorUpperLeftWall());
		obstructedTiles.put(TilesType.WET_UPPER_RIGHT_WALL, new WetFloorUpperRightWall());
		obstructedTiles.put(TilesType.WET_UPPER_WALL, new WetFloorUpperWall());
		
		obstructedTiles.put(TilesType.STAIR_LEFT, new StairLeft());
		obstructedTiles.put(TilesType.STAIR_LEFT_LEFT_WALL, new StairLeftLeftWall());
		obstructedTiles.put(TilesType.STAIR_LEFT_RIGHT_WALL, new StairLeftRightWall());
		
		obstructedTiles.put(TilesType.STAIR_RIGHT, new StairRight());
		obstructedTiles.put(TilesType.STAIR_RIGHT_LEFT_WALL, new StairRightLeftWall());
		obstructedTiles.put(TilesType.STAIR_RIGHT_RIGHT_WALL, new StairRightRightWall());
		
		obstructedTiles.put(TilesType.STAIR_UPPER, new StairUpper());
		obstructedTiles.put(TilesType.STAIR_UPPER_LEFT_WALL, new StairUpperLeftWall());
		obstructedTiles.put(TilesType.STAIR_UPPER_RIGHT_WALL, new StairUpperRightWall());
		
		obstructedTiles.put(TilesType.STAIR_LOWER, new StairLower());
		obstructedTiles.put(TilesType.STAIR_LOWER_LEFT_WALL, new StairLowerLeftWall());
		obstructedTiles.put(TilesType.STAIR_LOWER_RIGHT_WALL, new StairLowerRightWall());
		
		obstructedTiles.put(TilesType.DOOR_HORIZONTAL_CLOSED, new DoorHorizontalClosed());
		obstructedTiles.put(TilesType.DOOR_HORIZONTAL_OPEN, new DoorHorizontalOpen());
		
		obstructedTiles.put(TilesType.DOOR_VERTICAL_CLOSED, new DoorVerticalClosed());
		obstructedTiles.put(TilesType.DOOR_VERTICAL_OPEN, new DoorVerticalOpen());
		//obstructedTiles.put(TilesType.UNKNOWN, new Unknown());

		
		for(TilesType t : obstructedTiles.keySet()){
			obstructedTiles.get(t).setObstacle();
		}
		
	}
	
	public static TileFactory Instance(){
		
		if(instance == null)
			instance = new TileFactory();
		
		return instance;
	}
	
	public static Tiles getTile(String tileName){

		for(TilesType t : TileFactory.Instance().cleanTiles.keySet()){
			if(TileFactory.Instance().cleanTiles.get(t).toString().equals(tileName))
				return TileFactory.Instance().cleanTiles.get(t);
		}
		return null;
	}
	
	public static Tiles getTileSucio(String tileName){

		for(TilesType t : TileFactory.Instance().obstructedTiles.keySet()){
			if(TileFactory.Instance().obstructedTiles.get(t).toString().equals(tileName))
				return TileFactory.Instance().obstructedTiles.get(t);
		}
		return null;
	}
	
	public static Tiles getTile(TilesType t, boolean clean){
		if(clean || t.equals(TilesType.UNKNOWN)){
			return TileFactory.Instance().cleanTiles.get(t);
		}else{
			return TileFactory.Instance().obstructedTiles.get(t);
		}
	}
}
