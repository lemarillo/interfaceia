package propietary.tiles;

public class StairLowerRightWall extends Tiles {
	StairLowerRightWall(){
		super();
		this.velocityCostUp = 1.5f;
		this.velocityCostDown = 6;
		this.velocityCostLeft = 3;
		this.velocityCostRight = 3;
		this.allowMoveDown = true;
		this.allowMoveUp = true;
		this.allowMoveLeft = false;
		this.allowMoveRight = true;

	}
	
	@Override
	public String toString(){
		return "SLoRW";
	}
}
