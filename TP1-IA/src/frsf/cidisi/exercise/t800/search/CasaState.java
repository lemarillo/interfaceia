package frsf.cidisi.exercise.t800.search;

import java.util.ArrayList;

import propietary.classes.Config;
import propietary.classes.Door;
import propietary.classes.Position;
import propietary.tiles.TileFactory;
import propietary.tiles.Tiles;
import propietary.tiles.TilesType;


import frsf.cidisi.faia.state.EnvironmentState;

/**
 * This class represents the real world state.
 */
public class CasaState extends EnvironmentState {
	
	//TODO: Setup Variables
    //private int[][] World;
	private ArrayList<ArrayList<Tiles>> world;
    private Position sarahConnorPosition;
    private Position t800Position;
    private char t800Orientation;
	
    public CasaState() {
        
        //TODO: Complete Method
    	/*
			// World = initData0;
			// SarahConnorPosition = initData1;
			// T800Position = initData2;
			// GrafoAmbientes = initData3;
        */
    	world = new ArrayList<ArrayList<Tiles>>();
        this.initState();
    }

    /**
     * This method is used to setup the initial real world.
     */
    @Override
    public void initState() {

        //TODO: Complete Method
    	this.t800Orientation= Config.instance().getT800Orientation();
    	this.t800Position = Config.instance().getT800Position();
    	this.sarahConnorPosition = Config.instance().getSarahConnorPosition();
    	this.world = Config.instance().getWorld();
    	
    	ArrayList <Position> pos= Config.instance().getObstacles();
    	for(Position p : pos){
		Tiles limpio = this.world.get(p.getY()).get(p.getX());
		ArrayList<Tiles> row = this.world.get(p.getY());
		row.set(p.getX(), TileFactory.getTileSucio(limpio.toString()));
		this.world.set(p.getY(), row);
    	}
 
    }

    /**
     * String representation of the real world state.
     */
    @Override
    public String toString() {
        String str = "";

        //TODO: Complete Method

        return str;
    }

	//TODO: Complete this section with agent-specific methods
    // The following methods are agent-specific:
	
     public ArrayList<ArrayList<Tiles>> getWorld(){
        return world;
     }
     public void setWorld(ArrayList<ArrayList<Tiles>> arg){
        world = arg;
     }
     public Position getSarahConnorPosition(){
        return sarahConnorPosition;
     }
     public void setSarahConnorPosition(Position arg){
        sarahConnorPosition = arg;
     }
     public Position getT800Position(){
        return t800Position;
     }
     public void setT800Position(Position arg){
        t800Position = arg;
     }

     public char getT800Orientation() {
 		return t800Orientation;
 	}
     public void setT800Orientation(char o) {
 		this.t800Orientation=o;
 	}

	public Tiles getTileAt(Position pos){
    	 try{
    		 return world.get(pos.getY()).get(pos.getX());
    	 }catch(IndexOutOfBoundsException ex){
    		 return TileFactory.getTile(TilesType.UNKNOWN,false);
    	 }
     }
	
     
//     public Other getGrafoAmbientes(){
//        return GrafoAmbientes;
//     }
//     public void setGrafoAmbientes(Other arg){
//        GrafoAmbientes = arg;
//     }
	

}

