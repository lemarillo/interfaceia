package frsf.cidisi.exercise.t800.search;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import propietary.classes.Config;
import propietary.classes.ConoDeVision;
import propietary.classes.ConoNode;
import propietary.classes.Door;
import propietary.classes.Position;
import propietary.tiles.TileFactory;
import propietary.tiles.Tiles;
import propietary.tiles.TilesType;
import frsf.cidisi.faia.agent.Perception;
import frsf.cidisi.faia.agent.search.SearchBasedAgentState;

/**
 * Represent the internal state of the Agent.
 */
public class T800State extends SearchBasedAgentState {
	
	//TODO: Setup Variables
    private Tiles[][] world;
    private boolean[][] visitados;

    //private Position InitialPosition; //
    private Position position;
    //private int Velocity;
    private int ambiente;
    private char orientation;
    private int column;
    private int row;
    //private boolean sarahConnorFound;
    private T800AgentPerception lastPerception;

    public T800State() {
    
    	this.column = Config.instance().getColumn();
    	this.row = Config.instance().getRow();
    	this.world = new Tiles[row][column];
    	this.visitados = new boolean[row][column];
    	lastPerception = null;
        this.initState();
    }

    /**
     * This method clones the state of the agent. It's used in the search
     * process, when creating the search tree.
     */
    @Override
    public SearchBasedAgentState clone() {
        
		//TODO: Complete Method
//    	T800State newT800 = new T800State();
//    	newT800.ambiente = this.ambiente;
//    	newT800.orientation = this.orientation;
//    	newT800.position = this.position.clone();
//    	newT800.lastPerception = this.lastPerception.clone();
//    	
//    	
//    	for(int i = 0; i<row;i++){
//    		for(int j = 0; j<column; j++){
//    			newT800.world[i][j] = this.world[i][j]; //porque es un puntero
//    			newT800.visitados[i][j]= this.visitados[i][j];
//    		}
//    	}
//    	
//		
//        return newT800;
    	T800State newT800 = new T800State();
    	newT800.ambiente = this.ambiente;
    	newT800.orientation = this.orientation;
    	newT800.position = this.position.clone();
    	newT800.lastPerception = this.lastPerception;
    	
    	
    	for(int i = 0; i<row;i++){
    		for(int j = 0; j<column; j++){
    			newT800.world[i][j] = this.world[i][j]; //porque es un puntero
    			newT800.visitados[i][j]= this.visitados[i][j];
    		}
    	}
    	
		
        return newT800;
    }

    /**
     * This method is used to update the Agent State when a Perception is
     * received by the Simulator.
     */
    @Override
    public void updateState(Perception p) {
    	this.lastPerception = (T800AgentPerception)p;
    	
    	ConoDeVision cono = this.lastPerception.getconodevision();
    	
    	ConoNode[][] listaConoNodes = cono.leerCono();
    	
    	for(ConoNode[] fila : listaConoNodes){
    		for(ConoNode cn : fila){
    			Tiles tileConoNode = cn.getTile();
    			
    			if(tileConoNode.equals(TileFactory.getTile(TilesType.UNKNOWN, false)) == false){
    				Position tilePosition = cn.getPosition();
    				
    				if(this.world[tilePosition.getY()][tilePosition.getX()].equals(tileConoNode) == false){
    					this.world[tilePosition.getY()][tilePosition.getX()] = tileConoNode;
    				}
    				//if(cn.hasSarahConnor())
    					//this.sarahConnorFound = true;
    			}
    		}
    	}
    	
        //TODO: Complete Method
    	
    }

    /**
     * This method is optional, and sets the initial state of the agent.
     */
    @Override
    public void initState() {
	//TODO: Complete Method
    	this.position = Config.instance().getT800Position();
    	this.ambiente = Config.instance().getT800ambient();
    	this.orientation=Config.instance().getT800Orientation();
    	
    	for(int i= 0; i < row; i++){
    		for(int j= 0; j < column;j++){
    			this.world[i][j] = TileFactory.getTile(TilesType.UNKNOWN,false);
    		}
    	}
    }

    /**
     * This method returns the String representation of the agent state.
     */
    @Override
    public String toString() {
        String str = "";

        //TODO: Complete Method

        return str;
    }

    /**
     * This method is used in the search process to verify if the node already
     * exists in the actual search.
     */
    @Override
    public boolean equals(Object obj) {
       
       //TODO: Complete Method
    	if(obj.getClass() == this.getClass()){
    		T800State o = (T800State) obj;
    		if((this.ambiente == o.ambiente) && (this.position.equals(o.position)) && (this.orientation == o.orientation)){
    			boolean sameWorld = true;
    			int i = 0;
    			int j = 0;
    			while(sameWorld && j < column){
    				while(sameWorld && i < row){
    					sameWorld =  (o.visitados[i][j] == this.visitados[i][j])&& (this.world[i][j].equals(o.world[i][j]));
    					i++;
    				}
    				j++;
    			}
    			
    			if(sameWorld && this.lastPerception.equals(o.lastPerception)){
    				return true;
    			}
    		}
    	}
        
        return false;
    }

    //TODO: Complete this section with agent-specific methods
    // The following methods are agent-specific:
   	
     public Tiles[][] getWorld(){
        return world;
     }
     public void setWorld(Tiles[][] arg){
        world = arg;
     }
     /*public Position getInitialPosition(){
        return InitialPosition;
     }
     public void setInitialPosition(Position arg){
        InitialPosition = arg;
     }*/
     public Position getPosition(){
        return position;
     }
     //public void setPosition(Position arg){
     //   position = arg;
     //}
     /*public int getVelocity(){
        return Velocity;
     }
     public void setVelocity(int arg){
        Velocity = arg;
     }*/
     public int getAmbiente(){
        return ambiente;
     }
     public void setAmbiente(int arg){
        ambiente = arg;
     }

	public char getOrientation() {
		return orientation;
	}

	public void setOrientation(char orientation) {
		this.orientation = orientation;
	}
    public Tiles getMyTile(){
    	return world[this.position.getY()][this.position.getX()]; //revisar 
    }
    public Tiles getNextTile(){
    	switch(this.orientation){
    	case'N':
    		return world[this.position.getY() - 1][this.position.getX()];
    	case'E':
    		return world[this.position.getY()][this.position.getX() + 1];
    	case'S':
    		return world[this.position.getY() + 1][this.position.getX()];
    	case'W':
    		return world[this.position.getY()][this.position.getX() - 1];
    	default:
    		return TileFactory.getTile(TilesType.UNKNOWN, false);
    	}
    }
    public boolean nextMoveIsWithinBounds(){
    	Position nextpos = this.getNextPosition();
    	if(nextpos.getX() > 0 && nextpos.getX()<Config.instance().getColumn()){
    		if(nextpos.getY() > 0 && nextpos.getY() < Config.instance().getRow()){
    			return true;
    		}
    	}
    	return false;
    }
    private  Position getNextPosition(){
    	switch(this.orientation){
    	case'N':
    		return new Position(this.position.getX(), this.position.getY() - 1);
    	case'E':
    		return new Position(this.position.getX() + 1, this.position.getY());
    	case'S':
    		return new Position(this.position.getX(),this.position.getY() + 1);
    	case'W':
    		return new Position(this.position.getX() - 1, this.position.getY());
    	default:
    		return new Position(this.position.getX(), this.position.getY());
    	}
    }
//     public Other getListaAmbientes(){
//        return ListaAmbientes;
//     }
//     public void setListaAmbientes(Other arg){
//        ListaAmbientes = arg;
//     }

	public void visited() {
		visitados[this.position.getY()][this.position.getX()] = true;
		
	}
	
	public boolean nextWasvisited(){
		Position next = getNextPosition();
		return this.visitados[next.getY()][next.getX()];
	}
	
	public T800AgentPerception getLastPerception(){
		return lastPerception;
	}
	
}

