package frsf.cidisi.exercise.t800.search;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import propietary.classes.Config;
import propietary.classes.LectoWriter;
import frsf.cidisi.faia.exceptions.PrologConnectorException;
import frsf.cidisi.faia.simulator.SearchBasedAgentSimulator;

public class T800AgentMain {

    public static void main(String[] args) throws PrologConnectorException {
    	
    	Config.instance();
    	
        T800Agent agent = new T800Agent();

        CasaEnviroment environment = new CasaEnviroment();

        SearchBasedAgentSimulator simulator =
                new SearchBasedAgentSimulator(environment, agent);
        
        simulator.start();
        
    	
    }

}
