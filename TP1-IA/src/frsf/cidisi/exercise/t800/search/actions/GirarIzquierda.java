package frsf.cidisi.exercise.t800.search.actions;

import frsf.cidisi.exercise.t800.search.*;
import frsf.cidisi.faia.agent.search.SearchAction;
import frsf.cidisi.faia.agent.search.SearchBasedAgentState;
import frsf.cidisi.faia.state.AgentState;
import frsf.cidisi.faia.state.EnvironmentState;

public class GirarIzquierda extends SearchAction {

    /**
     * This method updates a tree node state when the search process is running.
     * It does not updates the real world state.
     */
    @Override
    public SearchBasedAgentState execute(SearchBasedAgentState s) {
        T800State agState = (T800State) s;
        
        // TODO: Use this conditions
        // PreConditions: null
        // PostConditions: null
          
        switch(agState.getOrientation()){
        case'N':
        	agState.setOrientation('W');
        	break;
        case'E':
        	agState.setOrientation('N');
        	break;
        case'S':
        	agState.setOrientation('E');
        	break;
        case'W':
        	agState.setOrientation('S');
        	break;
        }
        
        return agState;
    }

    /**
     * This method updates the agent state and the real world state.
     */
    @Override
    public EnvironmentState execute(AgentState ast, EnvironmentState est) {
        CasaState environmentState = (CasaState) est;
        T800State agState = ((T800State) ast);

        // TODO: Use this conditions
        // PreConditions: null
        // PostConditions: null
        
        if (true) {
            // Update the real world
        	switch(agState.getOrientation()){
            case'N':
            	agState.setOrientation('W');
            	break;
            case'E':
            	agState.setOrientation('N');
            	break;
            case'S':
            	agState.setOrientation('E');
            	break;
            case'W':
            	agState.setOrientation('S');
            	break;
            }
            // Update the agent state
            
            return environmentState;
        }

        return null;
    }

    /**
     * This method returns the action cost.
     */
    @Override
    public Double getCost() {
        return new Double(2);
    }

    /**
     * This method is not important for a search based agent, but is essensial
     * when creating a calculus based one.
     */
    @Override
    public String toString() {
        return "GirarIzquierda";
    }
}