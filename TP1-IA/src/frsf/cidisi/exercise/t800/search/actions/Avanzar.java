package frsf.cidisi.exercise.t800.search.actions;

import java.io.IOException;

import propietary.classes.LectoWriter;
import propietary.tiles.TileFactory;
import propietary.tiles.Tiles;
import propietary.tiles.TilesType;
import frsf.cidisi.exercise.t800.search.*;
import frsf.cidisi.faia.agent.search.SearchAction;
import frsf.cidisi.faia.agent.search.SearchBasedAgentState;
import frsf.cidisi.faia.state.AgentState;
import frsf.cidisi.faia.state.EnvironmentState;

public class Avanzar extends SearchAction {

    /**
     * This method updates a tree node state when the search process is running.
     * It does not updates the real world state.
     */
    @Override
    public SearchBasedAgentState execute(SearchBasedAgentState s) {
        T800State agState = (T800State) s;
        
        // TODO: Use this conditions
        // PreConditions: null
        // PostConditions: null
        
        char agentOrientation = agState.getOrientation();
        
        if(agState.getMyTile().canMove(agentOrientation) && agState.nextMoveIsWithinBounds()){
        	Tiles nextTile = agState.getNextTile();
        	if(!nextTile.hasObstacle() && !agState.nextWasvisited() /*&& !nextTile.equals(TileFactory.getTile(TilesType.UNKNOWN, false))*/){
        		switch(agState.getOrientation()){
            	case'N':
            		agState.getPosition().changePosition(agState.getPosition().getX(), agState.getPosition().getY() - 1);
            		break;
            	case'E':
            		agState.getPosition().changePosition(agState.getPosition().getX() + 1, agState.getPosition().getY());
            		break;
            	case'S':
            		agState.getPosition().changePosition(agState.getPosition().getX(), agState.getPosition().getY() + 1);
            		break;
            	case'W':
            		agState.getPosition().changePosition(agState.getPosition().getX() - 1, agState.getPosition().getY());
            		break;
            	}
        		//aca
        		//System.out.print(agState.getOrientation());
        		//System.out.println(agState.getPosition().toString()+" ");
        		return agState;
        	}
        }
        
        return null;
    }

    /**
     * This method updates the agent state and the real world state.
     */
    @Override
    public EnvironmentState execute(AgentState ast, EnvironmentState est) {
        CasaState environmentState = (CasaState) est;
        T800State agState = ((T800State) ast);

        // TODO: Use this conditions
        // PreConditions: null
        // PostConditions: null
        char agentOrientation = agState.getOrientation();
        
        if (agState.getMyTile().canMove(agentOrientation) && agState.nextMoveIsWithinBounds()) {
            // Update the real world
            // Update the agent state
        		agState.visited();
            	Tiles nextTile = agState.getNextTile();
            	
            	if(!nextTile.hasObstacle() /*&& !nextTile.equals(TileFactory.getTile(TilesType.UNKNOWN, false))*/){
            		switch(agState.getOrientation()){
                	case'N':
                		agState.getPosition().changePosition(agState.getPosition().getX(), agState.getPosition().getY() - 1);
                		break;
                	case'E':
                		agState.getPosition().changePosition(agState.getPosition().getX() + 1, agState.getPosition().getY());
                		break;
                	case'S':
                		agState.getPosition().changePosition(agState.getPosition().getX(), agState.getPosition().getY() + 1);
                		break;
                	case'W':
                		agState.getPosition().changePosition(agState.getPosition().getX() - 1, agState.getPosition().getY());
                		break;
                	}
            		try {
            	//		System.out.print(" "+ agState.getPosition().toString());
						LectoWriter.spitToFile(agState.getPosition().toString());
				//		LectoWriter.spitToFile("\n.............\n");

					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            		environmentState.setT800Position(agState.getPosition());
            		
            		return environmentState;
            	}
        }

        return null;
    }

    /**
     * This method returns the action cost.
     */
    @Override
    public Double getCost() {
        return new Double(1);
    }

    /**
     * This method is not important for a search based agent, but is essensial
     * when creating a calculus based one.
     */
    @Override
    public String toString() {
        return "Avanzar";
    }
}