package frsf.cidisi.exercise.t800.search;

import java.util.ArrayList;

import propietary.classes.Config;
import propietary.classes.ConoDeVision;
import propietary.classes.ConoNode;
import propietary.classes.Position;
import propietary.tiles.Tiles;



import frsf.cidisi.faia.agent.Agent;
import frsf.cidisi.faia.agent.Perception;
import frsf.cidisi.faia.environment.Environment;

public class T800AgentPerception extends Perception {

	//TODO: Setup Statics
    //public static int UNKNOWN_PERCEPTION = -1;   
	
	
	//TODO: Setup Sensors
	private ConoDeVision conodevision;
	//private int alerta;
	private int mensajedellamada;
	
 
	private T800AgentPerception(){
		
	}
    public  T800AgentPerception(CasaState cs) {
    	//TODO: Complete Method
    	//conodevision = new ArrayList<ConoNode>();
    	this.mensajedellamada = Config.instance().getSarahConnorAmbient();
    	this.conodevision = new ConoDeVision(cs.getT800Position(),Config.instance().getVisionLevel(),cs.getT800Orientation());
    }

    public T800AgentPerception(Agent agent, Environment environment) {
        super(agent, environment);
    }
    @Override
    public boolean equals(Object o ){
    	if(o.getClass().equals(this.getClass())){
    		T800AgentPerception ap= (T800AgentPerception) o;
    		return (this.mensajedellamada == ap.mensajedellamada) && this.conodevision.equals(ap.conodevision);
    		
    	}
    	return false;
    }
    @Override
    public T800AgentPerception clone(){
    	T800AgentPerception aux = new T800AgentPerception();
    	aux.mensajedellamada = this.mensajedellamada;
    	aux.conodevision = this.conodevision.clone();
    	
    	return aux;
    }

    /**
     * This method is used to setup the perception.
     */
    @Override
    public void initPerception(Agent agentIn, Environment environmentIn) {
    	
    	//TODO: Complete Method
        T800Agent agent = (T800Agent) agentIn;
        CasaEnviroment environment = (CasaEnviroment) environmentIn;
        CasaState environmentState = environment.getEnvironmentState();
        
        this.mensajedellamada = Config.instance().getSarahConnorAmbient();
        
        T800State state =  (T800State)agent.getAgentState();
        this.conodevision = new ConoDeVision(state.getPosition(),Config.instance().getVisionLevel(),state.getOrientation());
        //this.conodevision.add(new ConoNode(environmentState.getT800Position(),environmentState.getTileAt(environmentState.getT800Position())));
       
        
    }
    
    @Override
    public String toString() {
        StringBuffer str = new StringBuffer();

        //TODO: Complete Method (PORQUE?)

        return str.toString();
    }

    // The following methods are agent-specific:
    //TODO: Complete this section with the agent-specific methods
	
     public ConoDeVision getconodevision(){
        return conodevision;
     }
//     public void setconodevision(Other arg){
//        this.conodevision = arg;
//     }
     /*public int getalerta(){
        return alerta;
     }
     public void setalerta(int arg){
        this.alerta = arg;
     }*/
     public int getmensajedellamada(){
        return mensajedellamada;
     }
     public void setmensajedellamada(int arg){
        this.mensajedellamada = arg;
     }
     public boolean hasSarahConnor(){
    	return this.conodevision.hasSarahConnor();
     }
     
     public Position getSarahPosition(){
    	 return this.conodevision.getSarahPosition();
     }
	
   
}
