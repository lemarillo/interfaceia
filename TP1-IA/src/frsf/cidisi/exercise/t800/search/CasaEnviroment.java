package frsf.cidisi.exercise.t800.search;

import java.io.IOException;
import java.util.ArrayList;

import propietary.classes.Config;
import propietary.classes.ConoDeVision;
import propietary.classes.ConoNode;
import propietary.classes.LectoWriter;
import propietary.classes.Position;
import frsf.cidisi.faia.agent.Action;
import frsf.cidisi.faia.agent.Perception;
import frsf.cidisi.faia.environment.Environment;

public class CasaEnviroment extends Environment {

    public CasaEnviroment() {
        // Create the environment state
        this.environmentState = new CasaState();
    }

    public CasaState getEnvironmentState() {
        return (CasaState) super.getEnvironmentState();
    }

    /**
     * This method is called by the simulator. Given the Agent, it creates
     * a new perception reading, for example, the agent position.
     * @param agent
     * @return A perception that will be given to the agent by the simulator.
     */
    @Override
    public  T800AgentPerception getPercept() {
        // Create a new perception to return
         T800AgentPerception perception = new T800AgentPerception(this.getEnvironmentState());
		
		//TODO : Set the perceptions sensors
        
        
         char agentOrientation = this.getEnvironmentState().getT800Orientation();
         Position agentPosition = this.getEnvironmentState().getT800Position();
         
         perception.setmensajedellamada(Config.instance().getSarahConnorAmbient());
         
         //ConoDeVision conov = new ConoDeVision(agentPosition,Config.instance().getVisionLevel(),agentOrientation);
         
         
         ConoDeVision conoDeVision = perception.getconodevision();
         conoDeVision.agregarAlCono(new ConoNode(agentPosition,this.getEnvironmentState().getTileAt(agentPosition)));
         
         //conoDeVision.add(new ConoNode(agentPosition,this.getEnvironmentState().getTileAt(agentPosition)));
         
         Position pos00 = this.getPositionWithOffset(agentPosition, agentOrientation, 0, 1);
         conoDeVision.agregarAlCono(new ConoNode(pos00,this.getEnvironmentState().getTileAt(pos00)));
         
         Position pos1i1 = this.getPositionWithOffset(agentPosition, agentOrientation, -1, 2);
         Position pos10 = this.getPositionWithOffset(agentPosition, agentOrientation, 0, 2);
         Position pos1d1 = this.getPositionWithOffset(agentPosition, agentOrientation, 1, 2);
         conoDeVision.agregarAlCono(new ConoNode(pos1i1,this.getEnvironmentState().getTileAt(pos1i1)));
         conoDeVision.agregarAlCono(new ConoNode(pos10,this.getEnvironmentState().getTileAt(pos10)));
         conoDeVision.agregarAlCono(new ConoNode(pos1d1,this.getEnvironmentState().getTileAt(pos1d1)));
         
//         try {
//			LectoWriter.spitToFile(conoDeVision.toString());
//			LectoWriter.spitToFile("\n.............\n");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
         
//         Position pos2i2 = this.getPositionWithOffset(agentPosition, agentOrientation, -2, 3);
//         Position pos2i1 = this.getPositionWithOffset(agentPosition, agentOrientation, -1, 3);
//         Position pos20 = this.getPositionWithOffset(agentPosition, agentOrientation, 0,3);
//         Position pos2d1 = this.getPositionWithOffset(agentPosition, agentOrientation, 1, 3);
//         Position pos2d2 = this.getPositionWithOffset(agentPosition, agentOrientation, 2, 3);
//         conoDeVision.agregarAlCono(new ConoNode(pos2i2,this.getEnvironmentState().getTileAt(pos2i2)));
//         conoDeVision.agregarAlCono(new ConoNode(pos2i1,this.getEnvironmentState().getTileAt(pos2i1)));
//         conoDeVision.agregarAlCono(new ConoNode(pos20,this.getEnvironmentState().getTileAt(pos20)));
//         conoDeVision.agregarAlCono(new ConoNode(pos2d1,this.getEnvironmentState().getTileAt(pos2d1)));
//         conoDeVision.agregarAlCono(new ConoNode(pos2d2,this.getEnvironmentState().getTileAt(pos2d2)));
         
         // Return the perception
        return perception;
    }

    
    public String toString() {
        return environmentState.toString();
    }

    
    public boolean agentFailed(Action actionReturned) {

        CasaState envState =
                this.getEnvironmentState();

        // TODO: Complete Method        

        return false;
    }
    
    private Position getPositionWithOffset(Position pos, char orient, int offLateral, int offVertical){
//    	switch(orient){
//    	case 'N':
//    		return new Position(pos.getX() + offLateral,pos.getY() - 1 - offVertical);
//    	case 'E':
//    		return new Position(pos.getX()+ 1 + offVertical ,pos.getY()+ offLateral) ;
//    	case 'S':
//    		return new Position(pos.getX() - offLateral,pos.getY() + 1 + offVertical);
//    	case 'W':
//    		return new Position(pos.getX() - 1 - offVertical,pos.getY() - offLateral);
//    	default:
//    		return new Position(-1,-1);	//Si llega ac� estamos mal :c
//    	}
    	switch(orient){
    	case 'N':
    		return new Position(pos.getX() + offLateral,pos.getY()  - offVertical);
    	case 'E':
    		return new Position(pos.getX() + offVertical ,pos.getY()+ offLateral) ;
    	case 'S':
    		return new Position(pos.getX() - offLateral,pos.getY() + offVertical);
    	case 'W':
    		return new Position(pos.getX()  - offVertical,pos.getY() - offLateral);
    	default:
    		return new Position(-1,-1);	//Si llega ac� estamos mal :c
    	}
    }

	//TODO: Complete this section with agent-specific methods
    // The following methods are agent-specific:
    
    
}
