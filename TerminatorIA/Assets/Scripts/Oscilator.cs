﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscilator : MonoBehaviour {

    // Use this for initialization
    Vector3 scaleorig;

    [SerializeField]
    private float speed;
    [SerializeField]
    private float size;

    void Start () {
        scaleorig = this.transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
        var factor = size * Mathf.Sin(scaleorig.x * Time.time *speed);

        this.transform.localScale = new Vector3(scaleorig.x + factor, scaleorig.y + factor, scaleorig.z);


    }
}
