﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileFactory: MonoBehaviour {

    //private TileFactory instance;

    [SerializeField]
    private float tileOffset;
    [SerializeField]
    private Vector3 origin;

    [Header("Horizontal Door Tiles")]
    [SerializeField]
    private GameObject tileDHC;
    [SerializeField]
    private GameObject tileDHO;
    [Space(10)]
    [Header("Vertical Door Tiles")]
    [SerializeField]
    private GameObject tileDVC;
    [SerializeField]
    private GameObject tileDVO;
    [Space(10)]
    [Header("Grass Tiles")]
    [SerializeField]
    private GameObject tileG;
    [SerializeField]
    private GameObject tileGLeW;
    [SerializeField]
    private GameObject tileGLoLeW;
    [SerializeField]
    private GameObject tileGLoRW;
    [SerializeField]
    private GameObject tileGLoW;
    [SerializeField]
    private GameObject tileGRW;
    [SerializeField]
    private GameObject tileGULeW;
    [SerializeField]
    private GameObject tileGURW;
    [SerializeField]
    private GameObject tileGUW;
    [Space(10)]
    [Header("Normal Tiles")]
    [SerializeField]
    private GameObject tileN;
    [SerializeField]
    private GameObject tileNLeW;
    [SerializeField]
    private GameObject tileNLoLeW;
    [SerializeField]
    private GameObject tileNLoRW;
    [SerializeField]
    private GameObject tileNLoW;
    [SerializeField]
    private GameObject tileNRW;
    [SerializeField]
    private GameObject tileNULeW;
    [SerializeField]
    private GameObject tileNURW;
    [SerializeField]
    private GameObject tileNUW;
    [Space(10)]
    [Header("Rug Tiles")]
    [SerializeField]
    private GameObject tileR;
    [SerializeField]
    private GameObject tileRLeW;
    [SerializeField]
    private GameObject tileRLoLeW;
    [SerializeField]
    private GameObject tileRLoRW;
    [SerializeField]
    private GameObject tileRLoW;
    [SerializeField]
    private GameObject tileRRW;
    [SerializeField]
    private GameObject tileRULeW;
    [SerializeField]
    private GameObject tileRURW;
    [SerializeField]
    private GameObject tileRUW;
    [Space(10)]
    [Header("Wet Floor Tiles")]
    [SerializeField]
    private GameObject tileW;
    [SerializeField]
    private GameObject tileWLeW;
    [SerializeField]
    private GameObject tileWLoLeW;
    [SerializeField]
    private GameObject tileWLoRW;
    [SerializeField]
    private GameObject tileWLoW;
    [SerializeField]
    private GameObject tileWRW;
    [SerializeField]
    private GameObject tileWULeW;
    [SerializeField]
    private GameObject tileWURW;
    [SerializeField]
    private GameObject tileWUW;
    [Space(10)]
    [Header("Stair Left Tiles")]
    [SerializeField]
    private GameObject tileSLe;
    [SerializeField]
    private GameObject tileSLeLeW;
    [SerializeField]
    private GameObject tileSLeRW;
    [Space(10)]
    [Header("Stair Lower Tiles")]
    [SerializeField]
    private GameObject tileSLo;
    [SerializeField]
    private GameObject tileSLoLeW;
    [SerializeField]
    private GameObject tileSLoRW;
    [Space(10)]
    [Header("Stair Right Tiles")]
    [SerializeField]
    private GameObject tileSR;
    [SerializeField]
    private GameObject tileSRLeW;
    [SerializeField]
    private GameObject tileSRRW;
    [Space(10)]
    [Header("Stair Upper Tiles")]
    [SerializeField]
    private GameObject tileSU;
    [SerializeField]
    private GameObject tileSULeW;
    [SerializeField]
    private GameObject tileSURW;

    public GameObject SpawnSprite(string tileName, int row, int column)
    {
        GameObject aux = null;

        switch (tileName)
        {
            case "DHC":
                aux = Instantiate(tileDHC);
                break;
            case "DHO":
                aux = Instantiate(tileDHO);
                break;
            case "DVC":
                aux = Instantiate(tileDVC);
                break;
            case "DVO":
                aux = Instantiate(tileDVO);
                break;
            case "G":
                aux = Instantiate(tileG);
                break;
            case "GLeW":
                aux = Instantiate(tileGLeW);
                break;
            case "GLoLeW":
                aux = Instantiate(tileGLoLeW);
                break;
            case "GLoRW":
                aux = Instantiate(tileGLoRW);
                break;
            case "GLoW":
                aux = Instantiate(tileGLoW);
                break;
            case "GRW":
                aux = Instantiate(tileGRW);
                break;
            case "GULeW":
                aux = Instantiate(tileGULeW);
                break;
            case "GURW":
                aux = Instantiate(tileGURW);
                break;
            case "GUW":
                aux = Instantiate(tileGUW);
                break;
            case "N":
                aux = Instantiate(tileN);
                break;
            case "NLeW":
                aux = Instantiate(tileNLeW);
                break;
            case "NLoLeW":
                aux = Instantiate(tileNLoLeW);
                break;
            case "NLoRW":
                aux = Instantiate(tileNLoRW);
                break;
            case "NLoW":
                aux = Instantiate(tileNLoW);
                break;
            case "NRW":
                aux = Instantiate(tileNRW);
                break;
            case "NULeW":
                aux = Instantiate(tileNULeW);
                break;
            case "NURW":
                aux = Instantiate(tileNURW);
                break;
            case "NUW":
                aux = Instantiate(tileNUW);
                break;
            case "R":
                aux = Instantiate(tileR);
                break;
            case "RLeW":
                aux = Instantiate(tileRLeW);
                break;
            case "RLoLeW":
                aux = Instantiate(tileRLoLeW);
                break;
            case "RLoRW":
                aux = Instantiate(tileRLoRW);
                break;
            case "RLoW":
                aux = Instantiate(tileRLoW);
                break;
            case "RRW":
                aux = Instantiate(tileRRW);
                break;
            case "RULeW":
                aux = Instantiate(tileRULeW);
                break;
            case "RURW":
                aux = Instantiate(tileRURW);
                break;
            case "RUW":
                aux = Instantiate(tileRUW);
                break;
            case "W":
                aux = Instantiate(tileW);
                break;
            case "WLeW":
                aux = Instantiate(tileWLeW);
                break;
            case "WLoLeW":
                aux = Instantiate(tileWLoLeW);
                break;
            case "WLoRW":
                aux = Instantiate(tileWLoRW);
                break;
            case "WLoW":
                aux = Instantiate(tileWLoW);
                break;
            case "WRW":
                aux = Instantiate(tileWRW);
                break;
            case "WULeW":
                aux = Instantiate(tileWULeW);
                break;
            case "WURW":
                aux = Instantiate(tileWURW);
                break;
            case "WUW":
                aux = Instantiate(tileWUW);
                break;
            case "SLe":
                aux = Instantiate(tileSLe);
                break;
            case "SLeLeW":
                aux = Instantiate(tileSLeLeW);
                break;
            case "SLeRW":
                aux = Instantiate(tileSLeRW);
                break;
            case "SLo":
                aux = Instantiate(tileSLo);
                break;
            case "SLoLeW":
                aux = Instantiate(tileSLoLeW);
                break;
            case "SLoRW":
                aux = Instantiate(tileSLoRW);
                break;
            case "SR":
                aux = Instantiate(tileSR);
                break;
            case "SRLeW":
                aux = Instantiate(tileSRLeW);
                break;
            case "SRRW":
                aux = Instantiate(tileSRRW);
                break;
            case "SU":
                aux = Instantiate(tileSU);
                break;
            case "SULeW":
                aux = Instantiate(tileSULeW);
                break;
            case "SURW":
                aux = Instantiate(tileSURW);
                break;

        }

        aux.transform.position = new Vector3(origin.x + tileOffset * column, origin.y - tileOffset * row, origin.z);

        return aux;
    }

    public Vector3 getPositionOnIndex(float x, float y,float zoff)
    {
        return new Vector3(origin.x + tileOffset * x, origin.y - tileOffset * (y+1), zoff);
    }
}
