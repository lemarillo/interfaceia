﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    // Use this for initialization
    [SerializeField]
    GameObject obstacle;
    [SerializeField]
    Transform agent;
    [SerializeField]
    Transform sarahConnor;
    [SerializeField]
    float speed;
    [SerializeField]
    AudioSource music;

    private List<GameObject> worldMatrix;
    [SerializeField]
    private List<Vector2> agentPath;
    [SerializeField]
    private bool shouldMove;
    [SerializeField]
    private Vector2 nextStep;
    private int pathIndex;
    private int exitCode;

    [SerializeField]
    TileFactory factory;

    [SerializeField]
    Transform father;

    [SerializeField]
    float timeScale = 2f; //Cada cuantos segundos actualiza
    [SerializeField]
    float timeCounter;

    [SerializeField]
    public Text uiText;

    [SerializeField]
    bool autoPlay = false;
    [SerializeField]
    bool getNext = false;

    private int xagentpos = 0;
    private int yagentpos=0;

    private float zOffset = -1f; // para que aparezca adelante de ui

    private string output = "..\\results.csv";
    private string input = "..\\world.csv";

    private bool finishedStart;

    void Start() {
        timeCounter = timeScale;
        pathIndex = 0;
        agentPath = new List<Vector2>();
        worldMatrix = new List<GameObject>();
        finishedStart = false;
        shouldMove = false;

        var file = System.IO.File.Create(output);

        file.Close();

        string[] aux = System.IO.File.ReadAllLines(input);

        int size = aux.Length;

        for (int r = 1; r < size; r++)
        {
            string[] splitted = aux[r].Split(';');

            int splittedSize = splitted.Length;

            for (int c = 0; c < splittedSize; c++)
            {
                var newObject = factory.SpawnSprite(splitted[c], r, c);

                newObject.transform.SetParent(father);

                worldMatrix.Add(newObject);
            }
        }

        string obstacles = System.IO.File.ReadAllLines("..\\obstacles.csv")[1];
        string[] obscoma = obstacles.Split(new[] { ';', '(', ')' }, System.StringSplitOptions.RemoveEmptyEntries);

        foreach (string o in obscoma)
        {
            string[] oxy = o.Split(new[] { ',' }, System.StringSplitOptions.RemoveEmptyEntries);
            if (oxy.Length > 1)
            {
                int obsx = System.Int32.Parse(oxy[0].ToString());
                int obsy = System.Int32.Parse(oxy[1].ToString());

                var instobj = Instantiate(obstacle);
                instobj.transform.parent = father;
                instobj.transform.position = factory.getPositionOnIndex(obsx, obsy, zOffset);

            }

        }

        string[] javadata = System.IO.File.ReadAllLines("..\\javapathandjar.csv");

        //var processInfo =
        //    new ProcessStartInfo("C:\\Program Files (x86)\\Common Files\\Oracle\\Java\\javapath\\javaw.exe", "-jar C:\\Users\\leand\\Desktop\\iajava.jar")
        //    {
        //        CreateNoWindow = true,
        //        UseShellExecute = false
        //    };
        var processInfo =
            new ProcessStartInfo(javadata[0], javadata[1])
            {
                CreateNoWindow = true,
                UseShellExecute = false
            };

        Process proc;

        if ((proc = Process.Start(processInfo)) == null)
            throw new System.InvalidOperationException("No se pudo arrancar");

        proc.WaitForExit();
        exitCode = proc.ExitCode;
        proc.Close();

        UnityEngine.Debug.Log("Finalizo con codigo de error: " + exitCode);

        string outputList = System.IO.File.ReadAllLines(output)[0];
        //UnityEngine.Debug.Log(outputList.Length);
        string[] splittedlist = outputList.Split(';');

        foreach(string s in splittedlist)
        {
            if (s.Length > 0)
            {
                string[] pair = s.Split(new[] { ' ', ',','(',')' }, System.StringSplitOptions.RemoveEmptyEntries);

                //UnityEngine.Debug.Log(pair.Length);
                if (pair.Length > 1)
                {
                    //UnityEngine.Debug.Log(pair[0]);
                    //UnityEngine.Debug.Log(pair[1]);

                    int xval = System.Int32.Parse(pair[0].ToString());
                    int yval = System.Int32.Parse(pair[1].ToString());
                    
                    agentPath.Add(new Vector2(xval, yval));

                }
            }
        }

        //Leer posicion inicial del agente del archivo (TO DO);
        string positions = System.IO.File.ReadAllLines("..\\positions.csv")[1];
        string[] splittedpos = positions.Split(new[] { ' ', ';', ',', '(', ')' }, System.StringSplitOptions.RemoveEmptyEntries);
        if(splittedpos.Length > 3)
        {
            int n1 = System.Int32.Parse(splittedpos[0].ToString());
            int n2 = System.Int32.Parse(splittedpos[1].ToString());
            int n3 = System.Int32.Parse(splittedpos[2].ToString());
            int n4 = System.Int32.Parse(splittedpos[3].ToString());
            agent.transform.position = factory.getPositionOnIndex(n1, n2, zOffset);
            sarahConnor.transform.position = factory.getPositionOnIndex(n3, n4, zOffset);
        }



        finishedStart = true;
        music.Play();
        music.volume = 1;
        music.loop = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (finishedStart && pathIndex < this.agentPath.Count)
        {
            if (autoPlay && (shouldMove == false))
            {
                timeCounter += Time.deltaTime;

                if (timeCounter > timeScale && pathIndex < this.agentPath.Count)
                {
                    timeCounter = 0;

                    //UnityEngine.Debug.Log("Paso " + pathIndex + " : " + agentPath[pathIndex].x + " " + agentPath[pathIndex].y);
                    uiText.text = "Paso " + (pathIndex + 1) + " : (" + agentPath[pathIndex].x + ", " + agentPath[pathIndex].y + ")";
                    shouldMove = true;
                    nextStep = factory.getPositionOnIndex(agentPath[pathIndex].x, agentPath[pathIndex].y, zOffset);
                    pathIndex++;
                }
            }
            else
            {
                if (getNext && (shouldMove == false))
                {
                    getNext = false;
                    //UnityEngine.Debug.Log("Paso " + pathIndex + " : " + agentPath[pathIndex].x + " " + agentPath[pathIndex].y);
                    uiText.text = "Paso " + (pathIndex+1) + " : (" + agentPath[pathIndex].x + ", " + agentPath[pathIndex].y + ")";
                    shouldMove = true;
                    nextStep = factory.getPositionOnIndex(agentPath[pathIndex].x, agentPath[pathIndex].y,zOffset);                  
                    pathIndex++;
                }
                
            }
        }

        //Código para mover el agente;

        float error = 0.1f;


        if (shouldMove)
        {
            float step = speed * Time.deltaTime;
            agent.position = Vector3.MoveTowards(agent.position, nextStep, step);

            //UnityEngine.Debug.Log("Tengo que llegar a " + nextStep.x + " " + nextStep.y + " y estoy en " + agent.position.x + " " + agent.position.y);

            if (Vector3.Distance(agent.position, nextStep) < 0.1f)
            {
                agent.position = nextStep;
                shouldMove = false;
            }
        }
	}


    // Fin de ciclo de vida de unity;

    public void GetNextPressed()
    {
        if (getNext == false)
            getNext = true;
    }
    public void AutoPlayPressed()
    {
        if(autoPlay == false)
        {
            autoPlay = true;
        }
        else
        {
            autoPlay = false;
        }
    }

    public void translateToIndex(float x, float y)
    {
        
    }
}
