﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager
{
    private static Dictionary<InputAction, string> inputs =
        new Dictionary<InputAction, string>(){
            { InputAction.Submit, "Submit" },
            {InputAction.Cancel, "Cancel" },
            {InputAction.Pause, "Cancel" },
            {InputAction.Horizontal, "Horizontal" },
            {InputAction.Vertical, "Vertical" },
            {InputAction.Back, "Cancel" }
        };


    public static bool GetButtonDown(InputAction action)
    {
        if (inputs.ContainsKey(action) == false) return false;
        return Input.GetButtonDown(inputs[action]);
    }

    public static bool GetButtonUp(InputAction action)
    {
        if (inputs.ContainsKey(action) == false) return false;
        return Input.GetButtonUp(inputs[action]);
    }

    public static bool GetButton(InputAction action)
    {
        if (inputs.ContainsKey(action) == false) return false;
        return Input.GetButton(inputs[action]);
    }

    public static float GetAxis(InputAction actionAxis)
    {
        if (inputs.ContainsKey(actionAxis) == false) return 0;
        return Input.GetAxis(inputs[actionAxis]);
    }
}
