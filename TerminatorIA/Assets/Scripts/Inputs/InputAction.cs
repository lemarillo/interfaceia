﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputAction {

    None = 0,
    Submit = 1,
    Cancel = 2,
    Pause = 3,
    Horizontal = 4,
    Vertical = 5,
    Back = 6
}
