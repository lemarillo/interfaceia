﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Terminator.UI
{
    /// <summary>
    /// Esta clase sirve para pasar info de una clase a otra.
    /// </summary>
    public class Bundle
    {
        private readonly Dictionary<string, object> data;

        public Bundle()
        {
            this.data = new Dictionary<string, object>();
        }

        public T Get<T>(string key)
        {
            if (this.data.ContainsKey(key))
                return (T)this.data[key];
            return default(T);
        }

        public void Set(string key, object val)
        {
            this.data[key] = val;
        }

        public Bundle Clone()
        {
            var clone = new Bundle();
            foreach (var pair in this.data)
                clone.data.Add(pair.Key, pair.Value);
            return clone;
        }
    }
}
