﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Terminator.UI
{
    /// <summary>
    /// Clase encargada de la navegación entre pantallas. Enfocada a AssetsBundles, hay que generalizar para el caso que no se utilicen.
    /// </summary>
    public class Navigation : MonoBehaviour
    {
        private static Navigation self;

        [SerializeField]
        private RectTransform dialogs;

        [SerializeField]
        private RectTransform screens;

        [SerializeField]
        private GameObject errorPrefab; // Por si los mensdajes de error los mostramos así.

        [SerializeField]
        private GameObject tutorialPrefab; // Por si hay tutoriales más adelante.

        [SerializeField]
        private GameObject loadingPrefab; // Pantalla de login si la hay más adelante.

        [SerializeField]
        private GameObject firstViewPrefab; // La primer pantalla del juego.

        private Stack<UIView> vStack = new Stack<UIView>(); // Pila de pantallas del juego.

        private Selectable currentSelected; // Variable que se utiliza para implementar la navegación dentro de una pantalla.

        //--------------- Ciclo de Vida de Unity ------------//
        private void Awake()
        {
            self = this;

            Assert.IsNotNull(this.dialogs, "You must set the <dialogs> field");
            Assert.IsNotNull(this.screens, "You must set the <screens> field");

            if (this.firstViewPrefab != null)
                Navigate(this.firstViewPrefab);

            if (self.vStack.Peek().FirstSelected != null)
            {
                self.vStack.Peek().FirstSelected.Select();
                this.currentSelected = self.vStack.Peek().FirstSelected;
            }
        }

        private void Update()
        {
            if (InputManager.GetButtonDown(InputAction.Back)/* && !BlockPanel.Instance.IsBlocked*/)
                Back();

            var currentSeclect = EventSystem.current.currentSelectedGameObject;
            if (currentSeclect != null)
                this.currentSelected = currentSeclect.GetComponent<Selectable>();

            if (!Input.GetKey(KeyCode.LeftShift) && !(Input.GetKey(KeyCode.RightShift)) && Input.GetKeyUp(KeyCode.Tab))
            {
                if (this.currentSelected.navigation.selectOnDown != null)
                {
                    this.currentSelected.navigation.selectOnDown.Select();
                    this.currentSelected = this.currentSelected.navigation.selectOnDown;
                }
            }
            else if ((Input.GetKey(KeyCode.LeftShift) || (Input.GetKey(KeyCode.RightShift))) && Input.GetKeyUp(KeyCode.Tab))
            {
                if (this.currentSelected.navigation.selectOnUp != null)
                {
                    this.currentSelected.navigation.selectOnUp.Select();
                    this.currentSelected = this.currentSelected.navigation.selectOnUp;
                }
            }
        }
        //-------------- Fin ciclo de Vida de Unity -------------//


        /// <summary>
        /// Elimina la pila de llamadas de pantallas volviendo a la primer pantalla,
        /// y llamando a la pantalla pasada por parámetro si se le pasa una.
        /// </summary>
        /// <param name="navigationKey"> Pantalla a cargar, si no se decea cargar ninguna pasar NULL </param>
        /// <param name="bundle"> Por defecto es NULL. Es la información a enviar entre pantallas. </param>
        public static void CleanNavigate(string navigationKey, Bundle bundle = null)
        {
            var navigationNode = GetNavigationPrefab(navigationKey);

            self.Clear();

            if (navigationNode != null)
                self.Navigate(navigationNode.prefab, bundle);

        }

        /// <summary>
        /// Método que implementa la navegación a la siguienta pantalla o diálogo. Siempre y cuando se encuentre ya en el sistema local.
        /// Esto quiere decir que no se encuentre en un assetBundle.
        /// </summary>
        /// <param name="navigationKey"> Nombre de la siguiente pantalla a cargar. </param>
        /// <param name="bundle"> Informacion que se decea enviar a la siguiente pantalla. </param>
        public static void Navigate(string navigationKey, Bundle bundle = null)
        {
            var navigationNode = GetNavigationPrefab(navigationKey);

            if (navigationNode != null)
                self.Navigate(navigationNode.prefab, bundle);
        }

        /// <summary>
        /// Retorna el nodo de navegación requerido en base al nombre de pantalla que se requiere.
        /// </summary>
        /// <param name="navigationKey"> Nombre de la pantalla a la que se quiere acceder. </param>
        /// <returns> Prefab de la pantalla si existe un nodo con el nombre pasado por parámetro. </returns>
        private static NavigationNode GetNavigationPrefab(string navigationKey)
        {
            if (self == null || self.vStack == null || self.vStack.Count == 0)
                return null;

            var currentView = self.vStack.Peek();

            return currentView.NavigationNodes.FirstOrDefault(node => node.key == navigationKey);
        }

        /// <summary>
        /// Método que navega hacia otra pantalla que no se encuentre en el sistema local, se requiere un assetBundle, por lo que llama
        /// a una pantalla de Loading.
        /// </summary>
        /// <param name="assetBundle"> Nombre del assetBundle en el que se encuentra la pantalla. </param>
        /// <param name="prefabName"> Nombre de la pantalla que se decea cargar. </param>
        public static void NavigateAsync(string assetBundle, string prefabName = null)
        {
            NavigateAsync(assetBundle, new Bundle(), prefabName);
        }

        /// <summary>
        /// Método que navega hacia otra pantalla que no se encuentre en el sistema local, se requiere un assetBundle, por lo que llama
        /// a una pantalla de Loading. En este método además se puede pasar información a travez de las pantallas.
        /// </summary>
        /// <param name="assetBundle">Nombre del assetBundle en el que se encuentra la pantalla. </param>
        /// <param name="bundle"> Información a enviar entre pantallas. </param>
        /// <param name="prefabName"> Nombre de pantalla a cargar. </param>
        public static void NavigateAsync(string assetBundle, Bundle bundle, string prefabName = null)
        {
            bundle.Set(LoadingScreen.ASSET_BUNDLE_KEY, assetBundle);
            bundle.Set(LoadingScreen.PREFAB_NAME_KEY, prefabName ?? assetBundle);
            //bundle.Set(LoadingScreen.CALLBACK_SCREEN_PREFAB_KEY, self.currentScreenPrefab);

            self.Navigate(self.loadingPrefab, bundle);

        }

        /// <summary>
        /// Método que navega hacia diálogo de error.
        /// </summary>
        /// <param name="navigationKey"> Nombre del nodo de navegación del diálogo de error. </param>
        /// <param name="title"> Título del mensaje de error. </param>
        /// <param name="description"> Descripción del mensaje de error. </param>
        public static void NavigateError(string navigationKey, string title, string description = null)
        {
            //TODO: Descomentar si se utiliza mensajes de error de esta manera.
            var bundle = new Bundle();
            //var navigationNode = GetNavigationPrefab(navigationKey);

            //bundle.Set(ErrorDialog.TITLE_KEY, title);
            //bundle.Set(ErrorDialog.DESCRIPTION_KEY, description ?? "");
            //bundle.Set(ErrorDialog.CALLBACK_SCREEN_PREFAB_KEY, navigationNode.prefab ?? self.currentScreenPrefab);

            self.Navigate(self.errorPrefab, bundle);
        }

        /// <summary>
        /// Navega hacia el siguiente tutorial. (Deprecado)
        /// </summary>
        /// <param name="tutorialQueue"> Descripción del tutorial. </param>
        /// <returns></returns>
        //public static void NavigateTutorial(string description)
        //{
        //    var bundle = new Bundle();
        //    bundle.Set(TutorialDialog.TUTORIAL_KEY, description);

        //    self.Navigate(self.tutorialPrefab, bundle);
        //}

        /// <summary>
        /// Navega entre tutoriales de la cola.
        /// </summary>
        /// <param name="tutorialQueue"> Cola de tutoriales. </param>
        /// <returns> retorna verdadero si puede navegar, de lo contrario falso. </returns>
        public static bool NavigateTutorial(Queue<string> tutorialQueue)
        {
            var bundle = new Bundle();
            //bundle.Set(TutorialDialog.TUTORIAL_QUEUE, tutorialQueue);
            if (self.vStack.Peek().name.ToLower().Contains("tutorial"))
                return false;
            else
            {
                self.Navigate(self.tutorialPrefab, bundle);
                return true;
            }
        }

        /// <summary>
        /// Este método se utiliza si hay assets Bundles, pero es bastante malo este método y la mayoría de las veces innecesario.
        /// Lo que hace es ir hacia una pantalla anterior y recargar la siguente, ej: hay 3 pantallas: 1, 2, 3. actualmente se encuentra 
        /// abierta la pantalla 3, se llama al método, este carga la 1 y luego carga nuevamente la 2.
        /// </summary>
        /// <param name="assetBundle"> nombre del AssetBundle. </param>
        /// <param name="bundle"> Info a pasar entre niveles. </param>
        /// <param name="prefabName"> Nombre de prefab a instanciar al cargar el assetbundle. </param>
        public static void NavigateReplaceAsync(string assetBundle, Bundle bundle, string prefabName = null) //Reemplazar por un método más adecuado.
        {
            bundle.Set(LoadingScreen.ASSET_BUNDLE_KEY, assetBundle);
            bundle.Set(LoadingScreen.PREFAB_NAME_KEY, prefabName ?? assetBundle);

            var currentView = self.vStack.Peek();

            if (self.vStack.Count > 0)
                self.CloseTopView();

            if (!currentView.IsFullScreen && self.vStack.Count > 0)
                self.CloseTopView();

            self.Navigate(self.loadingPrefab, bundle);
        }

        /// <summary>
        /// implementa la navegación de pantallas. Método privado. encapsula el comportamiento de la Navegación.
        /// </summary>
        /// <param name="viewPrefab"> Prefab al que debe navegar. </param>
        /// <param name="bundle"> Info a pasar a travez de vistas. </param>
        private void Navigate(GameObject viewPrefab, Bundle bundle = null)
        {
            var viewGo = Instantiate(viewPrefab);
            var view = viewGo.GetComponentInChildren<UIView>(true);

            if (view == null)
            {
                print("Se navego hacia un prefab que no contiene vistas");
                return;
            }

            PushView(view, bundle?.Clone());
        }

        /// <summary>
        /// Retorna a la aterior vista de la pila de vistas.
        /// </summary>
        public static void Back(/*TutorialType tutorialKey = TutorialType.None*/)
        {
            if (self == null || self.vStack == null || self.vStack.Count <= 1)
                return;

            self.CloseTopView();

            //if (tutorialKey != TutorialType.None)
            //    Tutorial.instance.ShowTutorial(tutorialKey);
        }

        /// <summary>
        /// Borra la pila de listas.
        /// </summary>
        private void Clear()
        {
            while (this.vStack.Count > 1)
                this.CloseTopView();
        }

        /// <summary>
        /// Agrega la siguiente vista a la pila de vistas.
        /// </summary>
        /// <param name="view"> Vista a agregar. </param>
        /// <param name="bundle"> Información a pasar a la vista. </param>
        private void PushView(UIView view, Bundle bundle)
        {
            PauseTopView(view.IsFullScreen);

            var viewGo = view.gameObject;

            viewGo.transform.SetParent(view.IsFullScreen ? this.screens : this.dialogs, false);

            SetShadowForView(view);

            this.vStack.Push(view);
            view.OnCreate(bundle ?? new Bundle());

            if (view.FirstSelected != null)
            {
                view.FirstSelected.Select();
                this.currentSelected = view.FirstSelected;
            }
        }

        /// <summary>
        /// Pausa, si corresponde pausar, y cierra si se debe cerrar, la vista anterior.
        /// </summary>
        /// <param name="hide"></param>
        private void PauseTopView(bool hide = true)
        {
            if (this.vStack.Count == 0)
                return;

            var currentView = this.vStack.Peek();

            if (!currentView.IsFullScreen && hide) //Esto arregla un inconveniente que hay si se carga una pantalla desde un diálogo.
            {
                var lastView = this.vStack.ElementAt(1);

                lastView.gameObject.SetActive(false);
                lastView.OnPause();
            }

            if (currentView.IsStackable)
            {
                currentView.gameObject.SetActive(currentView.IsFullScreen && !hide);
                currentView.OnPause();
            }
            else
            {
                this.vStack.Pop();
                currentView.OnClose();
            }
        }

        /// <summary>
        /// Despausa la vista superior de la pila.
        /// </summary>
        private void ResumeTopView()
        {
            if (this.vStack.Count == 0)
                return;

            var currentView = this.vStack.Peek();

            SetShadowForView(currentView);

            if (currentView.FirstSelected != null)
            {
                currentView.FirstSelected.Select();
                this.currentSelected = currentView.FirstSelected;
            }

            currentView.gameObject.SetActive(true);
            currentView.OnResume();
        }

        /// <summary>
        /// Cierra la cista superior de la pila.
        /// </summary>
        private void CloseTopView()
        {
            if (this.vStack.Count == 0)
                return;

            var currentView = this.vStack.Pop();
            currentView.OnClose();

            ResumeTopView();
        }

        /// <summary>
        /// Si lo que se añade es un diálogo activa la sombra de diálogos.
        /// </summary>
        /// <param name="view"> Vista a la que se está llamando. </param>
        private void SetShadowForView(UIView view)
        {
            this.dialogs?.gameObject.SetActive(!view.IsFullScreen);

            if (!view.IsFullScreen && this.dialogs != null)
            {
                Image image = this.dialogs.GetComponent<Image>();
                image.color = new Color(0f, 0f, 0f, view.ShadowDarkness);
                image.raycastTarget = view.ShadowDarkness > 0f;
            }
        }
    }
}
