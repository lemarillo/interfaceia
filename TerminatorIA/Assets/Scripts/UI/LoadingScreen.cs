﻿//using AssetBundles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Terminator.UI
{
    /// <summary>
    /// Clase encargada de la pantalla de carga. Con assetBundle
    /// </summary>
    public class LoadingScreen : UIView
    {
        public static readonly string ASSET_BUNDLE_KEY = "AssetBundle";
        public static readonly string PREFAB_NAME_KEY = "PrefabName";
        //public static readonly string CALLBACK_SCREEN_PREFAB_KEY = "CallbackScreen";

        private static readonly string NEXT_VIEW_PREFAB_KEY = "NextView";

        private Bundle bundle;
        private string assetBundle;
        private string prefabName;
        //private AssetBundleLoadAssetOperation loadOperation;

        [SerializeField]
        private Text internetErrorText;

        [SerializeField]
        private Button reconnectButton;

        public override void OnCreate(Bundle bundle)
        {
            this.bundle = bundle;
            this.assetBundle = bundle.Get<string>(ASSET_BUNDLE_KEY);
            this.prefabName = bundle.Get<string>(PREFAB_NAME_KEY);
            //Debug.LogWarning(this.GetHashCode() + " Units : " + this.bundle.Get<List<CellUnitPayload>>("Units")?[0]?.Units.Count);
        }

        //al iniciar el ciclo de vida de unity se llama al start. Solo que en este caso es una corrutina. 
        private IEnumerator Start()
        {
            //this.loadOperation = AssetBundleManager.LoadAssetAsync(this.assetBundle, this.prefabName, typeof(GameObject));
            yield return new WaitForSeconds(1.5f);
            yield return Download();
        }

        /// <summary>
        /// Descara el asset bundle.
        /// </summary>
        /// <returns> AssetBundle Cargado en memoria. </returns>
        private IEnumerator Download()
        {
            //yield return this.loadOperation;
            yield return null;
            //Debug.LogWarning(this.GetHashCode() + " Units  Download : " + this.bundle.Get<List<CellUnitPayload>>("Units")?[0]?.Units.Count);

            //if (this.loadOperation.IsDone())
            //{
            //    var loadedAsset = this.loadOperation.GetAsset<GameObject>();
            //    Navigate(loadedAsset);
            //}
            //else
            //{
            //    //var callbackScreen = this.bundle.Get<GameObject>(CALLBACK_SCREEN_PREFAB_KEY);
            //    //Navigate(callbackScreen);
            //    Navigation.Back();
            //    print("Is NOT done");
            //}
        }

        /// <summary>
        /// Navega a la View deceada una vez descargado el asset bundle.
        /// </summary>
        /// <param name="view"> Vista a la que se decea navegar. </param>
        private void Navigate(GameObject view)
        {
            this.navigationNodes.Clear();
            this.navigationNodes.Add(new NavigationNode(NEXT_VIEW_PREFAB_KEY, view));

            Navigation.Navigate(NEXT_VIEW_PREFAB_KEY, this.bundle);

            //AssetBundleManager.UnloadAssetBundle(this.assetBundle);
        }
    }
}
