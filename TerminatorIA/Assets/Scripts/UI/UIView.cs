﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Terminator.UI
{
    /// <summary>
    /// Clase madre de todas las clases de la UI.
    /// </summary>
    public class UIView : MonoBehaviour
    {
        [SerializeField]
        protected List<NavigationNode> navigationNodes;// Nodos de navegación locales. Es decir no están en los assetBundles. Son las views a las que se puede ir desde esta view.

        public NavigationNode[] NavigationNodes
        {
            get { return this.navigationNodes.ToArray(); }
        }

        [SerializeField]
        private bool isFullScreen; //Es pantalla o diálogo?

        public bool IsFullScreen
        {
            get { return this.isFullScreen; }
        }

        [SerializeField]
        private bool isStackable = true; //Se guarda en la pila de pantallas?

        public bool IsStackable
        {
            get { return this.isStackable; }
        }

        [SerializeField]
        private float shadowDarkness = 0.5f; //Valor de la "sombra" de dialogos.

        public float ShadowDarkness
        {
            get { return this.shadowDarkness; }
            set { this.shadowDarkness = Mathf.Clamp01(value); }
        }

        [SerializeField]
        private Selectable firstSelected; //Primer elemento seleccionable de la pantalla. Para la navegación en pantallas.

        public Selectable FirstSelected
        {
            get { return this.firstSelected; }
        }

        /// <summary>
        /// Se ejecuta al crear la vista. IMPLEMENTABLE.
        /// </summary>
        /// <param name="bundle"> información que se pasa a la vista. </param>
        public virtual void OnCreate(Bundle bundle)
        {
        }

        /// <summary>
        /// Se ejecuta al pausar la vista. IMPLEMENTABLE.
        /// </summary>
        public virtual void OnPause()
        {
        }

        /// <summary>
        /// Se ejecuta al despausar la vista. IMPLEMENTABLE.
        /// </summary>
        public virtual void OnResume()
        {
        }

        /// <summary>
        /// Se ejecuta al cerrar la vista. IMPLEMENTABLE.
        /// </summary>
        public virtual void OnClose()
        {
            Destroy(this.gameObject);
        }
    }
}
