﻿using System;
using UnityEngine;

namespace Terminator.UI
{
    /// <summary>
    /// Son los nodos a los que se debe navegar desde una view a otra view.
    /// </summary>
    [Serializable]
    public class NavigationNode
    {
        public string key; 
        public GameObject prefab;

        public NavigationNode() : this(null, null)
        {
        }

        public NavigationNode(string key, GameObject prefab)
        {
            this.key = key;
            this.prefab = prefab;
        }
    }
}