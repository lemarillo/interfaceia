﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Terminator.UI
{
    /// <summary>
    /// Esta clase se incerta en un panel, para poder bloquear todos los inputs mientra se espera algo.
    /// </summary>
    public class BlockPanel : MonoBehaviour
    {
        [SerializeField]
        private GameObject loader;
        [SerializeField]
        private Color shadowColor;

        private Image shadowImage;
        public bool IsBlocked { get; private set; }
        public static BlockPanel Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            this.gameObject.SetActive(false);
            this.shadowImage = GetComponent<Image>();
            IsBlocked = false;
        }

        /// <summary>
        /// Bloquea todos los inputs.
        /// </summary>
        /// <param name="showShadow"> Se decea bloquear los inputs y mostrar una somra? </param>
        /// <param name="showLoader"> Se decea mostrar un loader? </param>
        public void Lock(bool showShadow = false, bool showLoader = false)
        {
            this.shadowImage.color = showShadow ? shadowColor : new Color(0, 0, 0, 0);
            this.loader.SetActive(showLoader);
            this.gameObject.SetActive(true);
            this.IsBlocked = true;
        }

        /// <summary>
        /// Desbloquea losinputs.
        /// </summary>
        public void Unlock()
        {
            this.gameObject.SetActive(false);
            this.IsBlocked = false;
        }
    }
}
