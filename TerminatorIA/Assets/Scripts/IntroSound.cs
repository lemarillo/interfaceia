﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroSound : MonoBehaviour {

    [SerializeField]
    AudioSource music;
    // Use this for initialization
    void Start () {
        music.Play();
        music.volume = 1;
        music.loop = true;
    }
	
	
}
